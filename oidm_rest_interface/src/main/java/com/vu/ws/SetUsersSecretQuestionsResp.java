
package com.vu.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for setUsersSecretQuestionsResp complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="setUsersSecretQuestionsResp">
 *   &lt;complexContent>
 *     &lt;extension base="{http://ws.vu.com/}oidmIntegrationReturnType">
 *       &lt;sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "setUsersSecretQuestionsResp")
public class SetUsersSecretQuestionsResp
    extends OidmIntegrationReturnType
{


}
