
package com.vu.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.vu.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetUsersSecretQuestionsResponse_QNAME = new QName("http://ws.vu.com/", "getUsersSecretQuestionsResponse");
    private final static QName _GetUsersInRole_QNAME = new QName("http://ws.vu.com/", "getUsersInRole");
    private final static QName _SetStaffExpiryDate_QNAME = new QName("http://ws.vu.com/", "setStaffExpiryDate");
    private final static QName _GetDefaultRole_QNAME = new QName("http://ws.vu.com/", "getDefaultRole");
    private final static QName _AdminChangePassword_QNAME = new QName("http://ws.vu.com/", "adminChangePassword");
    private final static QName _GetAdministratorRoles_QNAME = new QName("http://ws.vu.com/", "getAdministratorRoles");
    private final static QName _OIDMIntegrationFaultMessage_QNAME = new QName("http://ws.vu.com/", "OIDMIntegrationFaultMessage");
    private final static QName _UserChangePasswordResponse_QNAME = new QName("http://ws.vu.com/", "userChangePasswordResponse");
    private final static QName _GetSecretQuestionBank_QNAME = new QName("http://ws.vu.com/", "getSecretQuestionBank");
    private final static QName _CheckUserAnswersResponse_QNAME = new QName("http://ws.vu.com/", "checkUserAnswersResponse");
    private final static QName _ModifyUserRoleResponse_QNAME = new QName("http://ws.vu.com/", "modifyUserRoleResponse");
    private final static QName _ResetForgottenPassword_QNAME = new QName("http://ws.vu.com/", "resetForgottenPassword");
    private final static QName _AdminChangePasswordResponse_QNAME = new QName("http://ws.vu.com/", "adminChangePasswordResponse");
    private final static QName _PopulatingRoles_QNAME = new QName("http://ws.vu.com/", "populatingRoles");
    private final static QName _ModifyUser_QNAME = new QName("http://ws.vu.com/", "modifyUser");
    private final static QName _SetStaffExpiryDateResponse_QNAME = new QName("http://ws.vu.com/", "setStaffExpiryDateResponse");
    private final static QName _GetSecretQuestionParams_QNAME = new QName("http://ws.vu.com/", "getSecretQuestionParams");
    private final static QName _GetAdministratorRolesResponse_QNAME = new QName("http://ws.vu.com/", "getAdministratorRolesResponse");
    private final static QName _UserChangePassword_QNAME = new QName("http://ws.vu.com/", "userChangePassword");
    private final static QName _GetDefaultRoleResponse_QNAME = new QName("http://ws.vu.com/", "getDefaultRoleResponse");
    private final static QName _SetDefaultRoleResponse_QNAME = new QName("http://ws.vu.com/", "setDefaultRoleResponse");
    private final static QName _ResetForgottenPasswordResponse_QNAME = new QName("http://ws.vu.com/", "resetForgottenPasswordResponse");
    private final static QName _SetUsersSecretQuestionsResponse_QNAME = new QName("http://ws.vu.com/", "setUsersSecretQuestionsResponse");
    private final static QName _CheckUserAnswers_QNAME = new QName("http://ws.vu.com/", "checkUserAnswers");
    private final static QName _SetDefaultRole_QNAME = new QName("http://ws.vu.com/", "setDefaultRole");
    private final static QName _ModifyUserRole_QNAME = new QName("http://ws.vu.com/", "modifyUserRole");
    private final static QName _ModifyUserResponse_QNAME = new QName("http://ws.vu.com/", "modifyUserResponse");
    private final static QName _AddUserResponse_QNAME = new QName("http://ws.vu.com/", "addUserResponse");
    private final static QName _GetSecretQuestionBankResponse_QNAME = new QName("http://ws.vu.com/", "getSecretQuestionBankResponse");
    private final static QName _AddUser_QNAME = new QName("http://ws.vu.com/", "addUser");
    private final static QName _GetSecretQuestionParamsResponse_QNAME = new QName("http://ws.vu.com/", "getSecretQuestionParamsResponse");
    private final static QName _SetUsersSecretQuestions_QNAME = new QName("http://ws.vu.com/", "setUsersSecretQuestions");
    private final static QName _GetUsersInRoleResponse_QNAME = new QName("http://ws.vu.com/", "getUsersInRoleResponse");
    private final static QName _GetBusinessRoles_QNAME = new QName("http://ws.vu.com/", "getBusinessRoles");
    private final static QName _PopulatingRolesResponse_QNAME = new QName("http://ws.vu.com/", "populatingRolesResponse");
    private final static QName _GetBusinessRolesResponse_QNAME = new QName("http://ws.vu.com/", "getBusinessRolesResponse");
    private final static QName _GetUsersSecretQuestions_QNAME = new QName("http://ws.vu.com/", "getUsersSecretQuestions");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.vu.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetAdministratorRolesResp }
     * 
     */
    public GetAdministratorRolesResp createGetAdministratorRolesResp() {
        return new GetAdministratorRolesResp();
    }

    /**
     * Create an instance of {@link AdminChangePasswordResp }
     * 
     */
    public AdminChangePasswordResp createAdminChangePasswordResp() {
        return new AdminChangePasswordResp();
    }

    /**
     * Create an instance of {@link GetUsersSecretQuestions }
     * 
     */
    public GetUsersSecretQuestions createGetUsersSecretQuestions() {
        return new GetUsersSecretQuestions();
    }

    /**
     * Create an instance of {@link AddUserResponse }
     * 
     */
    public AddUserResponse createAddUserResponse() {
        return new AddUserResponse();
    }

    /**
     * Create an instance of {@link SetUsersSecretQuestionsResponse }
     * 
     */
    public SetUsersSecretQuestionsResponse createSetUsersSecretQuestionsResponse() {
        return new SetUsersSecretQuestionsResponse();
    }

    /**
     * Create an instance of {@link GetUsersInRoleResponse }
     * 
     */
    public GetUsersInRoleResponse createGetUsersInRoleResponse() {
        return new GetUsersInRoleResponse();
    }

    /**
     * Create an instance of {@link AddUser }
     * 
     */
    public AddUser createAddUser() {
        return new AddUser();
    }

    /**
     * Create an instance of {@link GetDefaultRoleResp }
     * 
     */
    public GetDefaultRoleResp createGetDefaultRoleResp() {
        return new GetDefaultRoleResp();
    }

    /**
     * Create an instance of {@link PopulatingRolesResponse }
     * 
     */
    public PopulatingRolesResponse createPopulatingRolesResponse() {
        return new PopulatingRolesResponse();
    }

    /**
     * Create an instance of {@link ModifyUserRole }
     * 
     */
    public ModifyUserRole createModifyUserRole() {
        return new ModifyUserRole();
    }

    /**
     * Create an instance of {@link SetUsersSecretQuestions }
     * 
     */
    public SetUsersSecretQuestions createSetUsersSecretQuestions() {
        return new SetUsersSecretQuestions();
    }

    /**
     * Create an instance of {@link GetSecretQuestionParams }
     * 
     */
    public GetSecretQuestionParams createGetSecretQuestionParams() {
        return new GetSecretQuestionParams();
    }

    /**
     * Create an instance of {@link GetAdministratorRolesResponse }
     * 
     */
    public GetAdministratorRolesResponse createGetAdministratorRolesResponse() {
        return new GetAdministratorRolesResponse();
    }

    /**
     * Create an instance of {@link ModifyUserRoleResp }
     * 
     */
    public ModifyUserRoleResp createModifyUserRoleResp() {
        return new ModifyUserRoleResp();
    }

    /**
     * Create an instance of {@link GetBusinessRolesResp }
     * 
     */
    public GetBusinessRolesResp createGetBusinessRolesResp() {
        return new GetBusinessRolesResp();
    }

    /**
     * Create an instance of {@link GetUsersSecretQuestionsResponse }
     * 
     */
    public GetUsersSecretQuestionsResponse createGetUsersSecretQuestionsResponse() {
        return new GetUsersSecretQuestionsResponse();
    }

    /**
     * Create an instance of {@link SetDefaultRoleResponse }
     * 
     */
    public SetDefaultRoleResponse createSetDefaultRoleResponse() {
        return new SetDefaultRoleResponse();
    }

    /**
     * Create an instance of {@link GetUsersInRole }
     * 
     */
    public GetUsersInRole createGetUsersInRole() {
        return new GetUsersInRole();
    }

    /**
     * Create an instance of {@link CheckUserAnswersResponse }
     * 
     */
    public CheckUserAnswersResponse createCheckUserAnswersResponse() {
        return new CheckUserAnswersResponse();
    }

    /**
     * Create an instance of {@link GetSecretQuestionParamsResponse }
     * 
     */
    public GetSecretQuestionParamsResponse createGetSecretQuestionParamsResponse() {
        return new GetSecretQuestionParamsResponse();
    }

    /**
     * Create an instance of {@link ModifyUserRoleResponse }
     * 
     */
    public ModifyUserRoleResponse createModifyUserRoleResponse() {
        return new ModifyUserRoleResponse();
    }

    /**
     * Create an instance of {@link GetBusinessRolesResponse }
     * 
     */
    public GetBusinessRolesResponse createGetBusinessRolesResponse() {
        return new GetBusinessRolesResponse();
    }

    /**
     * Create an instance of {@link PopulatingRolesResp }
     * 
     */
    public PopulatingRolesResp createPopulatingRolesResp() {
        return new PopulatingRolesResp();
    }

    /**
     * Create an instance of {@link AddUserResp }
     * 
     */
    public AddUserResp createAddUserResp() {
        return new AddUserResp();
    }

    /**
     * Create an instance of {@link AdminChangePasswordResponse }
     * 
     */
    public AdminChangePasswordResponse createAdminChangePasswordResponse() {
        return new AdminChangePasswordResponse();
    }

    /**
     * Create an instance of {@link UserManagementFaultType }
     * 
     */
    public UserManagementFaultType createUserManagementFaultType() {
        return new UserManagementFaultType();
    }

    /**
     * Create an instance of {@link GetSecretQuestionBankResponse }
     * 
     */
    public GetSecretQuestionBankResponse createGetSecretQuestionBankResponse() {
        return new GetSecretQuestionBankResponse();
    }

    /**
     * Create an instance of {@link Modifyuserbody }
     * 
     */
    public Modifyuserbody createModifyuserbody() {
        return new Modifyuserbody();
    }

    /**
     * Create an instance of {@link SetStaffExpiryDateResp }
     * 
     */
    public SetStaffExpiryDateResp createSetStaffExpiryDateResp() {
        return new SetStaffExpiryDateResp();
    }

    /**
     * Create an instance of {@link ResetForgottenPasswordResponse }
     * 
     */
    public ResetForgottenPasswordResponse createResetForgottenPasswordResponse() {
        return new ResetForgottenPasswordResponse();
    }

    /**
     * Create an instance of {@link GetSecretQuestionBank }
     * 
     */
    public GetSecretQuestionBank createGetSecretQuestionBank() {
        return new GetSecretQuestionBank();
    }

    /**
     * Create an instance of {@link CheckUserAnswersResp }
     * 
     */
    public CheckUserAnswersResp createCheckUserAnswersResp() {
        return new CheckUserAnswersResp();
    }

    /**
     * Create an instance of {@link GetSecretQuestionBankResp }
     * 
     */
    public GetSecretQuestionBankResp createGetSecretQuestionBankResp() {
        return new GetSecretQuestionBankResp();
    }

    /**
     * Create an instance of {@link CheckUserAnswers }
     * 
     */
    public CheckUserAnswers createCheckUserAnswers() {
        return new CheckUserAnswers();
    }

    /**
     * Create an instance of {@link PopulatingRoles }
     * 
     */
    public PopulatingRoles createPopulatingRoles() {
        return new PopulatingRoles();
    }

    /**
     * Create an instance of {@link AdminChangePassword }
     * 
     */
    public AdminChangePassword createAdminChangePassword() {
        return new AdminChangePassword();
    }

    /**
     * Create an instance of {@link SetDefaultRoleResp }
     * 
     */
    public SetDefaultRoleResp createSetDefaultRoleResp() {
        return new SetDefaultRoleResp();
    }

    /**
     * Create an instance of {@link UserChangePasswordResp }
     * 
     */
    public UserChangePasswordResp createUserChangePasswordResp() {
        return new UserChangePasswordResp();
    }

    /**
     * Create an instance of {@link GetAdministratorRoles }
     * 
     */
    public GetAdministratorRoles createGetAdministratorRoles() {
        return new GetAdministratorRoles();
    }

    /**
     * Create an instance of {@link UserChangePasswordResponse }
     * 
     */
    public UserChangePasswordResponse createUserChangePasswordResponse() {
        return new UserChangePasswordResponse();
    }

    /**
     * Create an instance of {@link GetUsersSecretQuestionsResp }
     * 
     */
    public GetUsersSecretQuestionsResp createGetUsersSecretQuestionsResp() {
        return new GetUsersSecretQuestionsResp();
    }

    /**
     * Create an instance of {@link UserRole }
     * 
     */
    public UserRole createUserRole() {
        return new UserRole();
    }

    /**
     * Create an instance of {@link OidmIntegrationReturnType }
     * 
     */
    public OidmIntegrationReturnType createOidmIntegrationReturnType() {
        return new OidmIntegrationReturnType();
    }

    /**
     * Create an instance of {@link GetBusinessRoles }
     * 
     */
    public GetBusinessRoles createGetBusinessRoles() {
        return new GetBusinessRoles();
    }

    /**
     * Create an instance of {@link ModifyUser }
     * 
     */
    public ModifyUser createModifyUser() {
        return new ModifyUser();
    }

    /**
     * Create an instance of {@link GetUsersInRoleResp }
     * 
     */
    public GetUsersInRoleResp createGetUsersInRoleResp() {
        return new GetUsersInRoleResp();
    }

    /**
     * Create an instance of {@link GetDefaultRoleResponse }
     * 
     */
    public GetDefaultRoleResponse createGetDefaultRoleResponse() {
        return new GetDefaultRoleResponse();
    }

    /**
     * Create an instance of {@link GetDefaultRole }
     * 
     */
    public GetDefaultRole createGetDefaultRole() {
        return new GetDefaultRole();
    }

    /**
     * Create an instance of {@link UserChangePassword }
     * 
     */
    public UserChangePassword createUserChangePassword() {
        return new UserChangePassword();
    }

    /**
     * Create an instance of {@link ResetForgottenPassword }
     * 
     */
    public ResetForgottenPassword createResetForgottenPassword() {
        return new ResetForgottenPassword();
    }

    /**
     * Create an instance of {@link SetUsersSecretQuestionsResp }
     * 
     */
    public SetUsersSecretQuestionsResp createSetUsersSecretQuestionsResp() {
        return new SetUsersSecretQuestionsResp();
    }

    /**
     * Create an instance of {@link SetDefaultRole }
     * 
     */
    public SetDefaultRole createSetDefaultRole() {
        return new SetDefaultRole();
    }

    /**
     * Create an instance of {@link ResetForgottenPasswordResp }
     * 
     */
    public ResetForgottenPasswordResp createResetForgottenPasswordResp() {
        return new ResetForgottenPasswordResp();
    }

    /**
     * Create an instance of {@link ModifyUserResponse }
     * 
     */
    public ModifyUserResponse createModifyUserResponse() {
        return new ModifyUserResponse();
    }

    /**
     * Create an instance of {@link GetSecretQuestionParamsResp }
     * 
     */
    public GetSecretQuestionParamsResp createGetSecretQuestionParamsResp() {
        return new GetSecretQuestionParamsResp();
    }

    /**
     * Create an instance of {@link ModifyUserResp }
     * 
     */
    public ModifyUserResp createModifyUserResp() {
        return new ModifyUserResp();
    }

    /**
     * Create an instance of {@link SetStaffExpiryDateResponse }
     * 
     */
    public SetStaffExpiryDateResponse createSetStaffExpiryDateResponse() {
        return new SetStaffExpiryDateResponse();
    }

    /**
     * Create an instance of {@link OIDMIntegrationFaultMessage }
     * 
     */
    public OIDMIntegrationFaultMessage createOIDMIntegrationFaultMessage() {
        return new OIDMIntegrationFaultMessage();
    }

    /**
     * Create an instance of {@link SetStaffExpiryDate }
     * 
     */
    public SetStaffExpiryDate createSetStaffExpiryDate() {
        return new SetStaffExpiryDate();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersSecretQuestionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getUsersSecretQuestionsResponse")
    public JAXBElement<GetUsersSecretQuestionsResponse> createGetUsersSecretQuestionsResponse(GetUsersSecretQuestionsResponse value) {
        return new JAXBElement<GetUsersSecretQuestionsResponse>(_GetUsersSecretQuestionsResponse_QNAME, GetUsersSecretQuestionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersInRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getUsersInRole")
    public JAXBElement<GetUsersInRole> createGetUsersInRole(GetUsersInRole value) {
        return new JAXBElement<GetUsersInRole>(_GetUsersInRole_QNAME, GetUsersInRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStaffExpiryDate }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "setStaffExpiryDate")
    public JAXBElement<SetStaffExpiryDate> createSetStaffExpiryDate(SetStaffExpiryDate value) {
        return new JAXBElement<SetStaffExpiryDate>(_SetStaffExpiryDate_QNAME, SetStaffExpiryDate.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDefaultRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getDefaultRole")
    public JAXBElement<GetDefaultRole> createGetDefaultRole(GetDefaultRole value) {
        return new JAXBElement<GetDefaultRole>(_GetDefaultRole_QNAME, GetDefaultRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdminChangePassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "adminChangePassword")
    public JAXBElement<AdminChangePassword> createAdminChangePassword(AdminChangePassword value) {
        return new JAXBElement<AdminChangePassword>(_AdminChangePassword_QNAME, AdminChangePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAdministratorRoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getAdministratorRoles")
    public JAXBElement<GetAdministratorRoles> createGetAdministratorRoles(GetAdministratorRoles value) {
        return new JAXBElement<GetAdministratorRoles>(_GetAdministratorRoles_QNAME, GetAdministratorRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OIDMIntegrationFaultMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "OIDMIntegrationFaultMessage")
    public JAXBElement<OIDMIntegrationFaultMessage> createOIDMIntegrationFaultMessage(OIDMIntegrationFaultMessage value) {
        return new JAXBElement<OIDMIntegrationFaultMessage>(_OIDMIntegrationFaultMessage_QNAME, OIDMIntegrationFaultMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserChangePasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "userChangePasswordResponse")
    public JAXBElement<UserChangePasswordResponse> createUserChangePasswordResponse(UserChangePasswordResponse value) {
        return new JAXBElement<UserChangePasswordResponse>(_UserChangePasswordResponse_QNAME, UserChangePasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSecretQuestionBank }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getSecretQuestionBank")
    public JAXBElement<GetSecretQuestionBank> createGetSecretQuestionBank(GetSecretQuestionBank value) {
        return new JAXBElement<GetSecretQuestionBank>(_GetSecretQuestionBank_QNAME, GetSecretQuestionBank.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckUserAnswersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "checkUserAnswersResponse")
    public JAXBElement<CheckUserAnswersResponse> createCheckUserAnswersResponse(CheckUserAnswersResponse value) {
        return new JAXBElement<CheckUserAnswersResponse>(_CheckUserAnswersResponse_QNAME, CheckUserAnswersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyUserRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "modifyUserRoleResponse")
    public JAXBElement<ModifyUserRoleResponse> createModifyUserRoleResponse(ModifyUserRoleResponse value) {
        return new JAXBElement<ModifyUserRoleResponse>(_ModifyUserRoleResponse_QNAME, ModifyUserRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetForgottenPassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "resetForgottenPassword")
    public JAXBElement<ResetForgottenPassword> createResetForgottenPassword(ResetForgottenPassword value) {
        return new JAXBElement<ResetForgottenPassword>(_ResetForgottenPassword_QNAME, ResetForgottenPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdminChangePasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "adminChangePasswordResponse")
    public JAXBElement<AdminChangePasswordResponse> createAdminChangePasswordResponse(AdminChangePasswordResponse value) {
        return new JAXBElement<AdminChangePasswordResponse>(_AdminChangePasswordResponse_QNAME, AdminChangePasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PopulatingRoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "populatingRoles")
    public JAXBElement<PopulatingRoles> createPopulatingRoles(PopulatingRoles value) {
        return new JAXBElement<PopulatingRoles>(_PopulatingRoles_QNAME, PopulatingRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "modifyUser")
    public JAXBElement<ModifyUser> createModifyUser(ModifyUser value) {
        return new JAXBElement<ModifyUser>(_ModifyUser_QNAME, ModifyUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetStaffExpiryDateResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "setStaffExpiryDateResponse")
    public JAXBElement<SetStaffExpiryDateResponse> createSetStaffExpiryDateResponse(SetStaffExpiryDateResponse value) {
        return new JAXBElement<SetStaffExpiryDateResponse>(_SetStaffExpiryDateResponse_QNAME, SetStaffExpiryDateResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSecretQuestionParams }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getSecretQuestionParams")
    public JAXBElement<GetSecretQuestionParams> createGetSecretQuestionParams(GetSecretQuestionParams value) {
        return new JAXBElement<GetSecretQuestionParams>(_GetSecretQuestionParams_QNAME, GetSecretQuestionParams.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetAdministratorRolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getAdministratorRolesResponse")
    public JAXBElement<GetAdministratorRolesResponse> createGetAdministratorRolesResponse(GetAdministratorRolesResponse value) {
        return new JAXBElement<GetAdministratorRolesResponse>(_GetAdministratorRolesResponse_QNAME, GetAdministratorRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UserChangePassword }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "userChangePassword")
    public JAXBElement<UserChangePassword> createUserChangePassword(UserChangePassword value) {
        return new JAXBElement<UserChangePassword>(_UserChangePassword_QNAME, UserChangePassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetDefaultRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getDefaultRoleResponse")
    public JAXBElement<GetDefaultRoleResponse> createGetDefaultRoleResponse(GetDefaultRoleResponse value) {
        return new JAXBElement<GetDefaultRoleResponse>(_GetDefaultRoleResponse_QNAME, GetDefaultRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDefaultRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "setDefaultRoleResponse")
    public JAXBElement<SetDefaultRoleResponse> createSetDefaultRoleResponse(SetDefaultRoleResponse value) {
        return new JAXBElement<SetDefaultRoleResponse>(_SetDefaultRoleResponse_QNAME, SetDefaultRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResetForgottenPasswordResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "resetForgottenPasswordResponse")
    public JAXBElement<ResetForgottenPasswordResponse> createResetForgottenPasswordResponse(ResetForgottenPasswordResponse value) {
        return new JAXBElement<ResetForgottenPasswordResponse>(_ResetForgottenPasswordResponse_QNAME, ResetForgottenPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUsersSecretQuestionsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "setUsersSecretQuestionsResponse")
    public JAXBElement<SetUsersSecretQuestionsResponse> createSetUsersSecretQuestionsResponse(SetUsersSecretQuestionsResponse value) {
        return new JAXBElement<SetUsersSecretQuestionsResponse>(_SetUsersSecretQuestionsResponse_QNAME, SetUsersSecretQuestionsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CheckUserAnswers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "checkUserAnswers")
    public JAXBElement<CheckUserAnswers> createCheckUserAnswers(CheckUserAnswers value) {
        return new JAXBElement<CheckUserAnswers>(_CheckUserAnswers_QNAME, CheckUserAnswers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDefaultRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "setDefaultRole")
    public JAXBElement<SetDefaultRole> createSetDefaultRole(SetDefaultRole value) {
        return new JAXBElement<SetDefaultRole>(_SetDefaultRole_QNAME, SetDefaultRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyUserRole }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "modifyUserRole")
    public JAXBElement<ModifyUserRole> createModifyUserRole(ModifyUserRole value) {
        return new JAXBElement<ModifyUserRole>(_ModifyUserRole_QNAME, ModifyUserRole.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModifyUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "modifyUserResponse")
    public JAXBElement<ModifyUserResponse> createModifyUserResponse(ModifyUserResponse value) {
        return new JAXBElement<ModifyUserResponse>(_ModifyUserResponse_QNAME, ModifyUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "addUserResponse")
    public JAXBElement<AddUserResponse> createAddUserResponse(AddUserResponse value) {
        return new JAXBElement<AddUserResponse>(_AddUserResponse_QNAME, AddUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSecretQuestionBankResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getSecretQuestionBankResponse")
    public JAXBElement<GetSecretQuestionBankResponse> createGetSecretQuestionBankResponse(GetSecretQuestionBankResponse value) {
        return new JAXBElement<GetSecretQuestionBankResponse>(_GetSecretQuestionBankResponse_QNAME, GetSecretQuestionBankResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "addUser")
    public JAXBElement<AddUser> createAddUser(AddUser value) {
        return new JAXBElement<AddUser>(_AddUser_QNAME, AddUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetSecretQuestionParamsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getSecretQuestionParamsResponse")
    public JAXBElement<GetSecretQuestionParamsResponse> createGetSecretQuestionParamsResponse(GetSecretQuestionParamsResponse value) {
        return new JAXBElement<GetSecretQuestionParamsResponse>(_GetSecretQuestionParamsResponse_QNAME, GetSecretQuestionParamsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetUsersSecretQuestions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "setUsersSecretQuestions")
    public JAXBElement<SetUsersSecretQuestions> createSetUsersSecretQuestions(SetUsersSecretQuestions value) {
        return new JAXBElement<SetUsersSecretQuestions>(_SetUsersSecretQuestions_QNAME, SetUsersSecretQuestions.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersInRoleResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getUsersInRoleResponse")
    public JAXBElement<GetUsersInRoleResponse> createGetUsersInRoleResponse(GetUsersInRoleResponse value) {
        return new JAXBElement<GetUsersInRoleResponse>(_GetUsersInRoleResponse_QNAME, GetUsersInRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessRoles }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getBusinessRoles")
    public JAXBElement<GetBusinessRoles> createGetBusinessRoles(GetBusinessRoles value) {
        return new JAXBElement<GetBusinessRoles>(_GetBusinessRoles_QNAME, GetBusinessRoles.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PopulatingRolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "populatingRolesResponse")
    public JAXBElement<PopulatingRolesResponse> createPopulatingRolesResponse(PopulatingRolesResponse value) {
        return new JAXBElement<PopulatingRolesResponse>(_PopulatingRolesResponse_QNAME, PopulatingRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetBusinessRolesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getBusinessRolesResponse")
    public JAXBElement<GetBusinessRolesResponse> createGetBusinessRolesResponse(GetBusinessRolesResponse value) {
        return new JAXBElement<GetBusinessRolesResponse>(_GetBusinessRolesResponse_QNAME, GetBusinessRolesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUsersSecretQuestions }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.vu.com/", name = "getUsersSecretQuestions")
    public JAXBElement<GetUsersSecretQuestions> createGetUsersSecretQuestions(GetUsersSecretQuestions value) {
        return new JAXBElement<GetUsersSecretQuestions>(_GetUsersSecretQuestions_QNAME, GetUsersSecretQuestions.class, null, value);
    }

}
