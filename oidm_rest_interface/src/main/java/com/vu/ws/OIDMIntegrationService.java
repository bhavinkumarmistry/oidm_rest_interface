package com.vu.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 2.1.8
 * Fri Mar 04 17:03:39 AEDT 2016
 * Generated source version: 2.1.8
 * 
 */
 
@WebService(targetNamespace = "http://ws.vu.com/", name = "OIDMIntegrationService")
@XmlSeeAlso({ObjectFactory.class})
public interface OIDMIntegrationService {

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "getBusinessRoles", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetBusinessRoles")
    @ResponseWrapper(localName = "getBusinessRolesResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetBusinessRolesResponse")
    @WebMethod
    public com.vu.ws.GetBusinessRolesResp getBusinessRoles() throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "getAdministratorRoles", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetAdministratorRoles")
    @ResponseWrapper(localName = "getAdministratorRolesResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetAdministratorRolesResponse")
    @WebMethod
    public com.vu.ws.GetAdministratorRolesResp getAdministratorRoles(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "getDefaultRole", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetDefaultRole")
    @ResponseWrapper(localName = "getDefaultRoleResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetDefaultRoleResponse")
    @WebMethod
    public com.vu.ws.GetDefaultRoleResp getDefaultRole(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "setUsersSecretQuestions", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.SetUsersSecretQuestions")
    @ResponseWrapper(localName = "setUsersSecretQuestionsResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.SetUsersSecretQuestionsResponse")
    @WebMethod
    public com.vu.ws.SetUsersSecretQuestionsResp setUsersSecretQuestions(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "mobile", targetNamespace = "")
        java.lang.String mobile,
        @WebParam(name = "email", targetNamespace = "")
        java.lang.String email,
        @WebParam(name = "qa", targetNamespace = "")
        java.util.List<java.lang.String> qa
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "getUsersInRole", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetUsersInRole")
    @ResponseWrapper(localName = "getUsersInRoleResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetUsersInRoleResponse")
    @WebMethod
    public com.vu.ws.GetUsersInRoleResp getUsersInRole(
        @WebParam(name = "roleName", targetNamespace = "")
        java.lang.String roleName
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "adminChangePassword", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.AdminChangePassword")
    @ResponseWrapper(localName = "adminChangePasswordResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.AdminChangePasswordResponse")
    @WebMethod
    public com.vu.ws.AdminChangePasswordResp adminChangePassword(
        @WebParam(name = "adminId", targetNamespace = "")
        java.lang.String adminId,
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "newPassword", targetNamespace = "")
        java.lang.String newPassword
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "addUser", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.AddUser")
    @ResponseWrapper(localName = "addUserResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.AddUserResponse")
    @WebMethod
    public com.vu.ws.AddUserResp addUser(
        @WebParam(name = "adminId", targetNamespace = "")
        java.lang.String adminId,
        @WebParam(name = "managerId", targetNamespace = "")
        java.lang.String managerId,
        @WebParam(name = "firstName", targetNamespace = "")
        java.lang.String firstName,
        @WebParam(name = "lastName", targetNamespace = "")
        java.lang.String lastName,
        @WebParam(name = "middleName", targetNamespace = "")
        java.lang.String middleName,
        @WebParam(name = "endOfContract", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar endOfContract,
        @WebParam(name = "costcentresHR", targetNamespace = "")
        java.lang.String costcentresHR,
        @WebParam(name = "costGroup", targetNamespace = "")
        java.lang.String costGroup,
        @WebParam(name = "campusIdHR", targetNamespace = "")
        java.lang.String campusIdHR,
        @WebParam(name = "extension", targetNamespace = "")
        java.lang.String extension,
        @WebParam(name = "personalEmail", targetNamespace = "")
        java.lang.String personalEmail,
        @WebParam(name = "orgUnitIdHR", targetNamespace = "")
        java.lang.String orgUnitIdHR
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "getSecretQuestionParams", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetSecretQuestionParams")
    @ResponseWrapper(localName = "getSecretQuestionParamsResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetSecretQuestionParamsResponse")
    @WebMethod
    public com.vu.ws.GetSecretQuestionParamsResp getSecretQuestionParams() throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "getSecretQuestionBank", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetSecretQuestionBank")
    @ResponseWrapper(localName = "getSecretQuestionBankResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetSecretQuestionBankResponse")
    @WebMethod
    public com.vu.ws.GetSecretQuestionBankResp getSecretQuestionBank() throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "modifyUserRole", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.ModifyUserRole")
    @ResponseWrapper(localName = "modifyUserRoleResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.ModifyUserRoleResponse")
    @WebMethod
    public com.vu.ws.ModifyUserRoleResp modifyUserRole(
        @WebParam(name = "adminId", targetNamespace = "")
        java.lang.String adminId,
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "roleName", targetNamespace = "")
        java.lang.String roleName,
        @WebParam(name = "endDate", targetNamespace = "")
        java.lang.String endDate,
        @WebParam(name = "startDate", targetNamespace = "")
        java.lang.String startDate,
        @WebParam(name = "defaultRoleflag", targetNamespace = "")
        boolean defaultRoleflag
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "resetForgottenPassword", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.ResetForgottenPassword")
    @ResponseWrapper(localName = "resetForgottenPasswordResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.ResetForgottenPasswordResponse")
    @WebMethod
    public com.vu.ws.ResetForgottenPasswordResp resetForgottenPassword(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "newPassword", targetNamespace = "")
        java.lang.String newPassword
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "setDefaultRole", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.SetDefaultRole")
    @ResponseWrapper(localName = "setDefaultRoleResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.SetDefaultRoleResponse")
    @WebMethod
    public com.vu.ws.SetDefaultRoleResp setDefaultRole(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "defaultRoleName", targetNamespace = "")
        java.lang.String defaultRoleName
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "populatingRoles", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.PopulatingRoles")
    @ResponseWrapper(localName = "populatingRolesResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.PopulatingRolesResponse")
    @WebMethod
    public com.vu.ws.PopulatingRolesResp populatingRoles(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "modifyUser", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.ModifyUser")
    @ResponseWrapper(localName = "modifyUserResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.ModifyUserResponse")
    @WebMethod
    public com.vu.ws.ModifyUserResp modifyUser(
        @WebParam(name = "arg0", targetNamespace = "")
        com.vu.ws.Modifyuserbody arg0
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "userChangePassword", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.UserChangePassword")
    @ResponseWrapper(localName = "userChangePasswordResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.UserChangePasswordResponse")
    @WebMethod
    public com.vu.ws.UserChangePasswordResp userChangePassword(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "currentPassword", targetNamespace = "")
        java.lang.String currentPassword,
        @WebParam(name = "newPassword", targetNamespace = "")
        java.lang.String newPassword
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "getUsersSecretQuestions", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetUsersSecretQuestions")
    @ResponseWrapper(localName = "getUsersSecretQuestionsResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.GetUsersSecretQuestionsResponse")
    @WebMethod
    public com.vu.ws.GetUsersSecretQuestionsResp getUsersSecretQuestions(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "checkUserAnswers", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.CheckUserAnswers")
    @ResponseWrapper(localName = "checkUserAnswersResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.CheckUserAnswersResponse")
    @WebMethod
    public com.vu.ws.CheckUserAnswersResp checkUserAnswers(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "usersAnswersQuestions", targetNamespace = "")
        java.util.List<java.lang.String> usersAnswersQuestions
    ) throws OIDMIntegrationFaultMessage_Exception;

    @WebResult(name = "return", targetNamespace = "")
    @RequestWrapper(localName = "setStaffExpiryDate", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.SetStaffExpiryDate")
    @ResponseWrapper(localName = "setStaffExpiryDateResponse", targetNamespace = "http://ws.vu.com/", className = "com.vu.ws.SetStaffExpiryDateResponse")
    @WebMethod
    public com.vu.ws.SetStaffExpiryDateResp setStaffExpiryDate(
        @WebParam(name = "userId", targetNamespace = "")
        java.lang.String userId,
        @WebParam(name = "expiryDate", targetNamespace = "")
        javax.xml.datatype.XMLGregorianCalendar expiryDate
    ) throws OIDMIntegrationFaultMessage_Exception;
}
