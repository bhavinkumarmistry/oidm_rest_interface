
package com.vu.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for oidmIntegrationReturnType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="oidmIntegrationReturnType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="success" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "oidmIntegrationReturnType", propOrder = {
    "message",
    "success"
})
@XmlSeeAlso({
    GetDefaultRoleResp.class,
    PopulatingRolesResp.class,
    AdminChangePasswordResp.class,
    GetUsersSecretQuestionsResp.class,
    SetUsersSecretQuestionsResp.class,
    GetSecretQuestionParamsResp.class,
    ResetForgottenPasswordResp.class,
    GetBusinessRolesResp.class,
    AddUserResp.class,
    CheckUserAnswersResp.class,
    ModifyUserResp.class,
    GetUsersInRoleResp.class,
    SetStaffExpiryDateResp.class,
    UserChangePasswordResp.class,
    GetAdministratorRolesResp.class,
    GetSecretQuestionBankResp.class,
    ModifyUserRoleResp.class,
    SetDefaultRoleResp.class
})
public class OidmIntegrationReturnType {

    protected String message;
    protected boolean success;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the success property.
     * 
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the value of the success property.
     * 
     */
    public void setSuccess(boolean value) {
        this.success = value;
    }

}
