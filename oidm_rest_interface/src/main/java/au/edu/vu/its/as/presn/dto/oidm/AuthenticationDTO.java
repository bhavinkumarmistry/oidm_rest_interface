/*
 *
 * File Name                      :  AuthenticationDTO.java
 * Creation/Modification History  :
 *
 * 19-Apr-10  Andrew RUAN  Created
 *
 */
package au.edu.vu.its.as.presn.dto.oidm;

import java.util.Arrays;

public class AuthenticationDTO implements java.io.Serializable {

   private PasswordDTO passwordStatus;
   private String      defaultRole;
   private RoleDTO[]   roles;
   private String lastPasswordChanger;


   public AuthenticationDTO() {}


   /**
    * Constractor
    */
   public AuthenticationDTO(PasswordDTO pPass) {
      setPasswordStatus(pPass);
      setRoles(new RoleDTO[0]);
   }


   /**
    * Returns passwordStatus value
    * 
    * @return <b>passwordStatus</b> passwordStatus value
    */
   public PasswordDTO getPasswordStatus() {
      return passwordStatus;
   }


   /**
    * Sets the value of passwordStatus
    * 
    * @param <b>pPasswordStatus</b> passwordStatus value
    */
   public void setPasswordStatus(PasswordDTO pPasswordStatus) {
      passwordStatus = pPasswordStatus;
   }


   /**
    * Returns defaultRole value
    * 
    * @return <b>defaultRole</b> defaultRole value
    */
   public String getDefaultRole() {
      return defaultRole;
   }


   /**
    * Sets the value of defaultRole
    * 
    * @param <b>pDefaultRole</b> defaultRole value
    */
   public void setDefaultRole(String pDefaultRole) {
      defaultRole = pDefaultRole;
   }


   /**
    * Returns roles value
    * 
    * @return <b>roles</b> roles value
    */
   public RoleDTO[] getRoles() {
      return roles;
   }


   /**
    * Sets the value of roles
    * 
    * @param <b>pRoles</b> roles value
    */
   public void setRoles(RoleDTO[] pRoles) {
      roles = pRoles;
   }

   public void setLastPasswordChanger(String pPasswordChanger) {
		this.lastPasswordChanger = pPasswordChanger;
	}


	public String getlastPasswordChanger() {
		return lastPasswordChanger;
	}

   /**
    * Returns String
    * 
    * @return <b>String</b> String
    */
   public String toString() {
      StringBuilder str = new StringBuilder();
      str.append("Authentication[");
      str.append("passwordStatus = ").append(passwordStatus);
      str.append(", defaultRole = ").append(defaultRole);
      str.append(", roles = ").append(Arrays.toString(roles));
      str.append(", lastPasswordChanger = ").append(lastPasswordChanger);
      str.append("]");
      return str.toString();
   }

}
