/*
*
* File Name                      :  RESTApplicationException.java
* Creation/Modification History  :
*
*   25-Jul-2013  K.Sivasuthan  Created
*   
*
*/

package au.edu.vu.its.as.ws.oidm;


import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import javax.ws.rs.WebApplicationException;


/**
 * Copyright (C) 2010-2011 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * REST Application Exception
 *
 * @author   : K.Sivasuthan
 * @version  : 1.0
 */

@Provider
public class RESTServiceApplicationExceptionHnadler implements ExceptionMapper<RESTApplicationException> {
	@Override
	public Response toResponse(RESTApplicationException exception) {
	    return Response.status(Status.BAD_REQUEST).entity(exception.getMessage() + " : " + exception.getCause().getMessage()).build(); 
	}
}
