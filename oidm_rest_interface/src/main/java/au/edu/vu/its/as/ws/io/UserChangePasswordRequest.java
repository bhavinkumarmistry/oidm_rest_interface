package au.edu.vu.its.as.ws.io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import au.edu.vu.its.as.ws.io.AbstractRequest;

@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserChangePasswordRequest  extends AbstractRequest {
	
	@XmlElement(name = "userId")
	private String userId;
	
	@XmlElement(name = "oldPassword")
	private String oldPassword;
	
	@XmlElement(name = "newPassword")
	private String newPassword;
	
	public UserChangePasswordRequest() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getUserId() {
		return userId;
	}
	
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	public String getOldPassword() {
		return oldPassword;
	}
	
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
}
