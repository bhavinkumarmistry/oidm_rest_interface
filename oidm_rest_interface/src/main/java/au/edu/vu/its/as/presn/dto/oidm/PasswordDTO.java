/*
 *
 * File Name                      :  PasswordDTO.java
 * Creation/Modification History  :
 *
 * 02-Mar-10  Andrew RUAN          Created
 * 19-Mar-10  Andrew RUAN          Added status, statuses
 * 29-Nov-10  Vladimir GLEIBERMAN  Added status TERMINATED
 * 20-Aug-12  Vladimir GLEIBERMAN  Added lastLogin
 *
 */
package au.edu.vu.its.as.presn.dto.oidm;

import java.util.Date;

public class PasswordDTO implements java.io.Serializable {

   public static enum Status {INVALIDPWD, ACTIVE, BLOCKED, LOCKED, WARNING, 
                              CHANGEPWD, TERMINATED, TERMINATEDEXT};
   
   private int    daysToRenew;
   private Status status;
   private Date   lastLogin;


   public PasswordDTO() {}


   /**
    * Returns daysToRenew value
    * 
    * @return <b>daysToRenew</b> daysToRenew value
    */
   public int getDaysToRenew() {
      return daysToRenew;
   }


   /**
    * Sets the value of daysToRenew
    * 
    * @param <b>pDaysToRenew</b> daysToRenew value
    */
   public void setDaysToRenew(int pDaysToRenew) {
      daysToRenew = pDaysToRenew;
   }


   /**
    * Returns status value
    *
    * @return <b>status</b> status value
    */
   public Status getStatus() {
      return status;
   }


   /**
    * Sets the value of status
    *
    * @param <b>pStatus</b> status value
    */
   public void setStatus(Status pStatus) {
      status = pStatus;
   }


   /**
    * Returns Last Login value
    *
    * @return <b>Date</b> Last Login value
    */
   public Date getLastLogin() {
      return lastLogin;
   }


   /**
    * Sets the value of Date
    *
    * @param <b>pLastLogin</b> status value
    */
   public void setLastLogin(Date pLastLogin) {
      lastLogin = pLastLogin;
   }


   /**
    * Returns String
    * 
    * @return <b>String</b> String
    */
   public String toString() {
      StringBuilder str = new StringBuilder();
      str.append("Password[");
      str.append("daysToRenew = ").append(daysToRenew);
      str.append(", status = ").append(status);
      str.append(", lastLogin = ").append(lastLogin);
      str.append("]");
      return str.toString();
   }
}
