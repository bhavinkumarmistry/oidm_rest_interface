package au.edu.vu.its.as.presn.dto.oidm;

import java.io.Serializable;

public class OIDMResultDTO implements Serializable{
      OIDMErrorDTO error;
      Object result;
      
      public OIDMErrorDTO getError() {
            return error;
      }
      public void setError(OIDMErrorDTO error) {
            this.error = error;
      }
      public Object getResult() {
            return result;
      }
      public void setResult(Object result) {
            this.result = result;
      }
      @Override
      public String toString() {
            return "OIDMResultDTO [error=" + String.valueOf(error) + ", result=" + String.valueOf(result) + "]";
      }
      
      
}
