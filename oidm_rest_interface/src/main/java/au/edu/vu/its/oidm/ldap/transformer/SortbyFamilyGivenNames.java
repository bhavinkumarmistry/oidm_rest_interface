package au.edu.vu.its.oidm.ldap.transformer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;
import org.mule.transformer.types.DataTypeFactory;

import au.edu.vu.its.as.presn.dto.oidm.StaffDTO;

public class SortbyFamilyGivenNames extends AbstractMessageTransformer {

	public SortbyFamilyGivenNames() {
		registerSourceType(DataTypeFactory.create(List.class, StaffDTO.class));
		setReturnDataType(DataTypeFactory.create(List.class, StaffDTO.class));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		List<StaffDTO> staffs = (List<StaffDTO>) message.getPayload();
		Collections.sort(staffs, sortByFamilyGivenNames());
		return staffs;
	}

	public Comparator<StaffDTO> sortByFamilyGivenNames() {
		return new Comparator<StaffDTO>() {
			public int compare(StaffDTO dto1, StaffDTO dto2) {
				if (null == dto1.getFamilyName()) {
					dto1.setFamilyName("");
				}
				if (null == dto2.getFamilyName()) {
					dto2.setFamilyName("");
				}
				int comp = dto1.getFamilyName().compareTo(dto2.getFamilyName());
				if (comp == 0) {
					if (null != dto1.getFirstName() && null != dto2.getFirstName()) {
						comp = dto1.getFirstName().compareTo(dto2.getFirstName());
					}
				}
				if (comp == 0) {
					if (null != dto1.getSecondName() && null != dto2.getSecondName()) {
						comp = dto1.getSecondName().compareTo(dto2.getSecondName());
					}
				}
				return comp;
			}
		};
	}
}
