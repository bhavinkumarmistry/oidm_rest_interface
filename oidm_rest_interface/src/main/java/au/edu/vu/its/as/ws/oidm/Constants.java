package au.edu.vu.its.as.ws.oidm;

public interface Constants {

	String METHOD="method";
	String USER_SESSION = "userSession";
	String USER_ID = "userId";
	String DEFAULT_ROLE = "defaultRole";
	String CHECK_DIGEST = "checkDigest";
	
}
