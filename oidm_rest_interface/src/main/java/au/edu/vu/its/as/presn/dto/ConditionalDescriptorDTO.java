/*
*
* File Name                      :  ConditionalDescriptorDTO.java
* Creation/Modification History  :
*
*  10-Apr-03  Chelladurai KANAGARATNAM  Created
*  05-Sep-06  Effie SHARMA              Added isValid, getMyValidDtos ()s
*
*/

package au.edu.vu.its.as.presn.dto;

import java.util.List;
import java.util.ArrayList;


/**
 * Copyright (C) 2006 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * This generic class can be used for any object which has two attributes .
 *
 * @author   : Vladimir GLEIBERMAN
 * @Version  : 5.0
 */

public class ConditionalDescriptorDTO extends DescriptorDTO 
                                      implements java.io.Serializable {
    
   private boolean[] usage;


   /**                           
    * Creates empty ConditionalDescriptorDTO 
    */                            
   public ConditionalDescriptorDTO() {       
   }                            


   /**                           
    * Creates populated ConditionalDescriptorDTO 
    * 
    * @param <b>pId</b> item Id
    * @param <b>pDesc</b> Item description
    * @param <b>pUsage</b> Usage array
    */                            
   public ConditionalDescriptorDTO(String pId, String pDesc, boolean[] pUsage) {
      setId(pId);
      setDescription(pDesc);
      setUsage(pUsage);
   }                            

    
   /** 
    * Returns Usage array 
    *
    * @return <b>boolean[]</b> Usage
    */ 
   public boolean[] getUsage() {
      return usage;
   }
    
    
   /** 
    * Sets Usage
    *
    * @param <b>pUsage</b> Usage array
    */ 
   public void setUsage(boolean[] pUsage) {
      usage = pUsage;
   }
    
   /** 
    * Whether or not current instance meets the required (passed) validation
    *
    * @param <b>pValidation</b> the validation required
    * @return <b>boolean</b> whether or not condition was met
    */ 
   public boolean isValid(Boolean[] pValidation) {
      for (int i=0, n=pValidation.length; i<n; i++) { //false if a condn not met
         if (null !=pValidation[i] && pValidation[i].booleanValue()!=usage[i]){
            return false;
         }
      }

      return true;  //not proven false, so must be true
   }
    
   /** 
    * Whether or not current instance meets the required (passed) validation
    *
    * @param <b>pDtos</b> the original dto list
    * @param <b>pValidation</b> the validation required
    * @return <b>ConditionalDescriptorDTO[]</b> dto list which met conditions
    */ 
   public ConditionalDescriptorDTO[] getMyValidDtos(
                    ConditionalDescriptorDTO[] pDtos, Boolean[] pValidation) {
      List conditionedDtos = new ArrayList();
      for (int i=0, n=pDtos.length; i<n; i++) { 
         if (pDtos[i].isValid(pValidation) ) {
            conditionedDtos.add(pDtos[i]);
         }
      }

      return (ConditionalDescriptorDTO[])conditionedDtos.toArray
                                              (new ConditionalDescriptorDTO[0]);
   } 
    
   /** 
    * Returns String 
    * @return <b>String</b> printable version of an instance
    */ 
   public String toString() {
      return super.toString() + " Usage# " + usage.length;
   }
}
