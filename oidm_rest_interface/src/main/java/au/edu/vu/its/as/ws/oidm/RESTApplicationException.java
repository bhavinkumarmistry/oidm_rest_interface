/*
*
* File Name                      :  RESTApplicationException.java
* Creation/Modification History  :
*
*   25-Jul-2013  K.Sivasuthan  Created
*   
*
*/

package au.edu.vu.its.as.ws.oidm;

import java.io.Serializable;

/**
 * Copyright (C) 2010-2011 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * REST Application Exception
 *
 * @author   : K.Sivasuthan
 * @version  : 1.0
 */
 
public class RESTApplicationException extends Exception implements Serializable {
	private static final long serialVersionUID = 1L;
	
    public RESTApplicationException() {
        super();
    }
    
    public RESTApplicationException(String msg)   {
        super(msg);
    }
    
    public RESTApplicationException(String msg, Exception e)  {
        super(msg, e);
    }
}