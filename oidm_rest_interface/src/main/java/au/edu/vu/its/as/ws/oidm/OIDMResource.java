/*
 *
 * File Name                      :  OIDMResource.java
 * Creation/Modification History  :
 *
 *   25-Jan-2016  K.Sivasuthan Created
 *   
 *
 */

package au.edu.vu.its.as.ws.oidm;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mule.api.ExceptionPayload;
import org.mule.api.MuleContext;
import org.mule.api.MuleMessage;
import org.mule.api.context.MuleContextAware;
import org.mule.module.client.MuleClient;
import org.w3c.dom.Element;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vu.ws.AddUserResp;
import com.vu.ws.AdminChangePasswordResp;
import com.vu.ws.CheckUserAnswersResp;
import com.vu.ws.GetAdministratorRolesResp;
import com.vu.ws.GetBusinessRolesResp;
import com.vu.ws.GetDefaultRoleResp;
import com.vu.ws.GetSecretQuestionBankResp;
import com.vu.ws.GetSecretQuestionParamsResp;
import com.vu.ws.GetUsersInRoleResp;
import com.vu.ws.GetUsersSecretQuestionsResp;
import com.vu.ws.ModifyUserResp;
import com.vu.ws.ModifyUserRoleResp;
import com.vu.ws.ResetForgottenPasswordResp;
import com.vu.ws.SetDefaultRoleResp;
import com.vu.ws.SetUsersSecretQuestionsResp;
import com.vu.ws.UserChangePasswordResp;

import au.edu.vu.its.as.presn.dto.ConditionalDescriptorDTO;
import au.edu.vu.its.as.presn.dto.DescriptorDTO;
import au.edu.vu.its.as.presn.dto.oidm.ForgotPasswordDTO;
import au.edu.vu.its.as.presn.dto.oidm.OIDMErrorDTO;
import au.edu.vu.its.as.presn.dto.oidm.OIDMResultDTO;
import au.edu.vu.its.as.presn.dto.oidm.StaffDTO;
import au.edu.vu.its.as.ws.io.AddUserRequest;
import au.edu.vu.its.as.ws.io.AdminChangePasswordRequest;
import au.edu.vu.its.as.ws.io.CheckUserAnswersRequest;
import au.edu.vu.its.as.ws.io.ModifyUserRequest;
import au.edu.vu.its.as.ws.io.ModifyUserRoleRequest;
import au.edu.vu.its.as.ws.io.ResetForgottenPasswordRequest;
import au.edu.vu.its.as.ws.io.SetDefaultRoleRequest;
import au.edu.vu.its.as.ws.io.SetUserSecretAnswersRequest;
import au.edu.vu.its.as.ws.io.UserChangePasswordRequest;
import au.edu.vu.its.as.ws.ldap.LDAPData;
import au.edu.vu.its.as.ws.mule.ClientPasswordCallback;
import au.edu.vu.its.as.ws.mule.MuleContextListener;
import au.edu.vu.its.as.ws.transformer.OidmToRestTransformer;
import au.edu.vu.its.as.ws.transformer.RestToOidmTransformer;
import au.edu.vu.its.as.ws.util.OIMUtil;

//

/**
 * Copyright (C) 2013 Victoria University, Melbourne, VIC AUS All Rights
 * Reserved.
 * 
 * D2LResource
 * 
 * @author : K.Sivasuthan
 * @version : 1.0
 */

/**
 * @author e5021708
 *
 */
/**
 * @author e5021708
 * 
 */
@Path("/oidm/1.0")
public class OIDMResource implements MuleContextAware {
	public static Element server = null;
	public static MuleClient muleClient;
	private static ObjectMapper mapper;
	private static Properties edsProps = new Properties();

	private static MuleContext muleCtx;
	private static SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

	private static Log logger = LogFactory.getLog(OIDMResource.class);

	static {
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,false);
		try {
			initEDSProps();
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error("EDS Properties initialization failed.", ex);
		}
		
		try {
    		initPasswords(); 		
    	} catch (Exception ex) {
    		ex.printStackTrace();
    		logger.error("password initialization failed.", ex);
    	}
	}

	@Override
	public void setMuleContext(MuleContext context) {
		muleCtx = context;

	}

	public static void initEDSProps() throws Exception {
		try {
			edsProps.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("ldap.properties"));
			System.out.println("EDS Properties initialised.");
		} catch (Exception xe) {
			System.out.println("EDS Properties initialisation failed" + xe);
			throw new Exception("EDS Properties initialisation failed", xe);
		}
	}

	/**
	 * @param pAccountId
	 * @return
	 * @throws RESTApplicationException
	 */
	@GET
	@Path("/roles/defaultRole")
	@Produces(MediaType.APPLICATION_JSON)
	public String getDefaultRole(@QueryParam(Constants.USER_ID) String pAccountId)throws RESTApplicationException {

		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "getDefaultRole");
		Object[] params = new Object[] { pAccountId };

		GetDefaultRoleResp response = (GetDefaultRoleResp) callOIDM(muleProps,params);

		try {
			if (response.isSuccess()) {
				result.setResult(response.getDefaultRole());
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	/**
	 * @param pUserSession
	 * @param pRequest
	 * @return
	 * @throws RESTApplicationException
	 */
	/**
	 * @param pUserSession
	 * @param pRequest
	 * @return
	 * @throws RESTApplicationException
	 */
	@POST
	@Path("/roles/setDefaultRole")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String setDefaultRole(@QueryParam(Constants.USER_SESSION) String pUserSession,SetDefaultRoleRequest pRequest) throws RESTApplicationException {

		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "setDefaultRole");
		Object[] params = new Object[] { pRequest.getUserId(),pRequest.getDefaultRole() };

		SetDefaultRoleResp response = (SetDefaultRoleResp) callOIDM(muleProps,params);
		try {
			if (response.isSuccess()) {
				result.setResult(response.getMessage());
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	/**
	 * @param pUserSession
	 * @return
	 * @throws RESTApplicationException
	 */
	@GET
	@Path("/qanda/secretQuestionBank")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getSecretQuestionBank(@QueryParam(Constants.USER_SESSION) String pUserSession)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "getSecretQuestionBank");
		Object[] params = new Object[] {};

		GetSecretQuestionBankResp response = (GetSecretQuestionBankResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
				ConditionalDescriptorDTO[] questions = OidmToRestTransformer.transformSecretQuestionBank(response);
				result.setResult(questions);
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	/**
	 * @param pUserSession
	 * @return
	 * @throws RESTApplicationException
	 */
	@GET
	@Path("/qanda/secretQuestionParams")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getSecretQuestionParams(@QueryParam(Constants.USER_SESSION) String pUserSession)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "getSecretQuestionParams");
		Object[] params = new Object[] {};
		System.out.println("11111111 - calling OIM......getSecretQuestionParams");
		GetSecretQuestionParamsResp response = (GetSecretQuestionParamsResp) callOIDM(muleProps, params);
		System.out.println("11111111 - calling OIM......getSecretQuestionParams..Got response");		
		try {
			if (response.isSuccess()) {
				DescriptorDTO[] questionParams = OidmToRestTransformer.transformSecretQuestionParams(response);
				result.setResult(questionParams);
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	/**
	 * @param pUserId
	 * @return
	 * @throws RESTApplicationException
	 */
	@GET
	@Path("/qanda/userSecretQuestions")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getUserSecretQuestions(@QueryParam(Constants.USER_ID) String pUserId)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "getUsersSecretQuestions");
		Object[] params = new Object[] { pUserId };

		GetUsersSecretQuestionsResp response = (GetUsersSecretQuestionsResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
				ForgotPasswordDTO userSecretQs = OidmToRestTransformer.transformUserSecretQuestions(response);
				result.setResult(userSecretQs);
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	/**
	 * @param pUserSession
	 * @param pRequest
	 * @return
	 * @throws RESTApplicationException
	 */
	@POST
	@Path("/qanda/userSecretAnswers")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String setUserSecretAnswers(@QueryParam(Constants.USER_SESSION) String pUserSession,@QueryParam(Constants.CHECK_DIGEST) String pCheckDigest,SetUserSecretAnswersRequest pRequest)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "setUsersSecretQuestions");

		if (!isDigestValid(new String[] {pRequest.getUserId(), pRequest.getMobile(), pRequest.getEmail()},pRequest.getDigest(), pCheckDigest)) {
			err.setCode("GE91");
			result.setError(err);
			try {
				return mapper.writeValueAsString(result);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
			}

		}
	
		Object[] params = new Object[] { pRequest.getUserId(),pRequest.getMobile(), pRequest.getEmail(),RestToOidmTransformer.createAnswersList(pRequest.getAnswers()) };

		SetUsersSecretQuestionsResp response = (SetUsersSecretQuestionsResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
				result.setResult(response.getMessage());
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	/**
	 * @param pRequest
	 * @return
	 * @throws RESTApplicationException
	 */
	@POST
	@Path("/pwd/userChangePassword")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String userChangePassword(UserChangePasswordRequest pRequest)throws RESTApplicationException {

		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "userChangePassword");
		Object[] params = new Object[] { pRequest.getUserId(),pRequest.getOldPassword(), pRequest.getNewPassword() };
		UserChangePasswordResp response = (UserChangePasswordResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
			} else {
				err = extractErrorPWCalls(err, response.getMessage(),muleProps, params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@POST
	@Path("pwd/adminChangePassword")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String adminChangePassword(@QueryParam(Constants.CHECK_DIGEST) String pCheckDigest, AdminChangePasswordRequest pRequest)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "adminChangePassword");
		Object[] params = new Object[] { pRequest.getAdminId(),pRequest.getUserId(), pRequest.getNewPassword() };

		
		if (!isDigestValid(new String[] { pRequest.getAdminId(), pRequest.getUserId(),pRequest.getNewPassword() }, pRequest.getDigest(), pCheckDigest)) {
			err.setCode("GE91");
			result.setError(err);
			try {
				return mapper.writeValueAsString(result);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
			}

		}
		 

		AdminChangePasswordResp response = (AdminChangePasswordResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
			} else {
				err = extractErrorPWCalls(err, response.getMessage(),muleProps, params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@POST
	@Path("/pwd/resetForgottenPassword")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String resetForgottenPassword(@QueryParam(Constants.CHECK_DIGEST) String pCheckDigest, ResetForgottenPasswordRequest pRequest)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();
		
		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "resetForgottenPassword");
		Object[] params = new Object[] { pRequest.getUserId(),pRequest.getNewPassword() };
		
		if (!isDigestValid(new String[] {pRequest.getUserId(), pRequest.getNewPassword()}, pRequest.getDigest(), pCheckDigest)) {
			err.setCode("GE91");
			result.setError(err);
			try {
				return mapper.writeValueAsString(result);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
			}

		}

		

		ResetForgottenPasswordResp response = (ResetForgottenPasswordResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
			} else {
				err = extractErrorPWCalls(err, response.getMessage(),muleProps, params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@POST
	@Path("/users/addUser")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String addUser(@QueryParam(Constants.CHECK_DIGEST) String pCheckDigest, AddUserRequest pRequest)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();
		Object[] params = null;

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "addUser");

		
		if (!isDigestValid(new String[] { pRequest.getAdminId(),pRequest.getStaff().getFirstName(),pRequest.getStaff().getFamilyName() }, pRequest.getDigest(), pCheckDigest)) {
			err.setCode("GE91");
			result.setError(err);
			try {
				return mapper.writeValueAsString(result);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
			}

		}
		

		try {
			params = RestToOidmTransformer.transformAddUser(pRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}

		AddUserResp response = (AddUserResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
				result.setResult(OidmToRestTransformer.transformAddUser(pRequest, response));
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@POST
	@Path("/users/modifyUser")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String modifyUser(@QueryParam(Constants.CHECK_DIGEST) String pCheckDigest, ModifyUserRequest pRequest)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();
		Object[] params = null;

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "modifyUser");

		
		if (!isDigestValid(new String[] { pRequest.getAdminId(),pRequest.getStaff().getFirstName(),pRequest.getStaff().getFamilyName() }, pRequest.getDigest(), pCheckDigest)) {
			err.setCode("GE91");
			result.setError(err);
			try {
				return mapper.writeValueAsString(result);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
			}

		}
		

		try {
			params = RestToOidmTransformer.transformModifyUser(pRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}

		ModifyUserResp response = (ModifyUserResp) callOIDM(muleProps, params);
		try {
			if (response.isSuccess()) {
				result.setResult(response.getMessage());
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@GET
	@Path("/roles/administratorRoles")
	@Produces(MediaType.APPLICATION_JSON)
	public String getAdministratorRoles(@QueryParam("adminId") String pAdminId)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "getAdministratorRoles");
		Object[] params = new Object[] { pAdminId };

		GetAdministratorRolesResp response = (GetAdministratorRolesResp) callOIDM(muleProps, params);

		try {
			if (response.isSuccess()) {
				result.setResult(response.getUserManagedRoles());
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@GET
	@Path("/roles/businessRoles")
	@Produces(MediaType.APPLICATION_JSON)
	public String getBusinessRoles() throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "getBusinessRoles");
		Object[] params = new Object[] {};

		GetBusinessRolesResp response = (GetBusinessRolesResp) callOIDM(muleProps, params);

		try {
			if (response.isSuccess()) {
				result.setResult(response.getBusinessRoles());
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@POST
	@Path("/roles/modifyUserRole")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String modifyUserRole(@QueryParam(Constants.CHECK_DIGEST) String pCheckDigest, ModifyUserRoleRequest pRequest)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();
		Object[] params = null;

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "modifyUserRole");

		
		 
		if (!isDigestValid(new String[] { pRequest.getAdminId(), pRequest.getUserId(),pRequest.getRole(), pRequest.getDateFrom(),pRequest.getDateTo(), pRequest.getDefault() },pRequest.getDigest(), pCheckDigest)) {
			err.setCode("GE91");
			result.setError(err);
			try {
				return mapper.writeValueAsString(result);
			} catch (Exception ex) {
				ex.printStackTrace();
				throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
			}

		}
		 

		try {
			params = RestToOidmTransformer.transformModifyUserRole(pRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}

		ModifyUserRoleResp response = (ModifyUserRoleResp) callOIDM(muleProps,
				params);
		try {
			if (response.isSuccess()) {
				result.setResult(response.getMessage());
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@POST
	@Path("/qanda/checkUserAnswers")
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes({ MediaType.APPLICATION_JSON })
	public String checkUserAnswers(CheckUserAnswersRequest pRequest)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();
		Object[] params = null;

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "checkUserAnswers");

		try {
			params = RestToOidmTransformer.transformCheckUserAnswers(pRequest);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}

		CheckUserAnswersResp response = (CheckUserAnswersResp) callOIDM(muleProps, params);

		try {
			Map<String, Boolean> resMap = new HashMap<String, Boolean>();
			resMap.put("isValid", response.isSuccess());

			if (response.isSuccess()) {
				result.setResult(resMap);
			} else {
				if (null != response.getMessage() && response.getMessage().contains("QA01")) {
					result.setResult(resMap);
				} else {
					err = extractError(err, response.getMessage(), muleProps,params);
				}
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	@GET
	@Path("/roles/{role}/users")
	@Produces({ MediaType.APPLICATION_JSON })
	public String getUsersInRole(@PathParam("role") String pRole)throws RESTApplicationException {
		OIDMErrorDTO err = new OIDMErrorDTO();
		OIDMResultDTO result = new OIDMResultDTO();

		Map<String, Object> muleProps = new HashMap<String, Object>();
		muleProps.put("operation", "getUsersInRole");
		Object[] params = new Object[] { pRole };

		GetUsersInRoleResp response = (GetUsersInRoleResp) callOIDM(muleProps,params);

		try {

			if (response.isSuccess()) {
				muleProps = new HashMap<String, Object>();
				muleProps.put("operation", "populatingRoles");

				List<StaffDTO> staffs = new ArrayList<StaffDTO>();

				for (String loginId : response.getUsersInRole()) {
					StaffDTO dto = new LDAPData().getStaffInfo(loginId,edsProps);

					/*
					 * 
					 * params = new Object[] { loginId }; // populating roles
					 * PopulatingRolesResp rolesResp = (PopulatingRolesResp)
					 * callOIDM( muleProps, params); if (rolesResp.isSuccess())
					 * { AuthenticationDTO authInfo = new AuthenticationDTO();
					 * authInfo.setDefaultRole(rolesResp.getDefaultRole());
					 * authInfo.setRoles(OidmToRestTransformer
					 * .extractRoles(rolesResp.getUserRoles()));
					 * dto.setAuthenticationInfo(authInfo); } else { err =
					 * extractError(err, rolesResp.getMessage()); }
					 */
					// populating personal email
					muleProps = new HashMap<String, Object>();
					muleProps.put("operation", "getUsersSecretQuestions");
					params = new Object[] { loginId };
					GetUsersSecretQuestionsResp usqResp = (GetUsersSecretQuestionsResp) callOIDM(muleProps, params);

					if (usqResp.isSuccess()) {
						dto.setPersonalEmail(usqResp.getPersonalEmail());
					} else {
						// err = extractError(err, usqResp.getMessage());
						logger.error("getUsersInRole() failed to find personal email for "+ loginId + " " + usqResp.getMessage());
					}

					staffs.add(dto);
					System.out.println("#### " + loginId + " " + dto);
				}

				Collections.sort(staffs, sortByFamilyGivenNames());

				result.setResult(staffs);
			} else {
				err = extractError(err, response.getMessage(), muleProps,params);
			}

			result.setError(err);

			return mapper.writeValueAsString(result);
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	private Object callOIDM(Map<String, Object> muleProps, Object[] params)
			throws RESTApplicationException {
		ExceptionPayload problem;
		try {
			MuleMessage dbResponse = MuleContextListener.muleClient.send("vm://oidmWSVM", params, muleProps);
			problem = dbResponse.getExceptionPayload();
			if (null != problem) {
				logger.error("Failed -" + muleProps.get("operation"),problem.getException());
				throw new RESTApplicationException("Failed - "+ muleProps.get("operation") + ".", new Exception(problem.getException()));
			}

			return dbResponse.getPayload();

		} catch (Exception ex) {
			ex.printStackTrace();
			throw new RESTApplicationException("Failed - "+ muleProps.get("operation"), ex);
		}
	}

	private OIDMErrorDTO extractError(OIDMErrorDTO err, String message,Map<String, Object> muleProps, Object[] params) {
		logger.error("Method: " + muleProps.get("operation") + " Input: "
				+ Arrays.toString(params) + " Error: " + message);
		if (null != message && message.length() > 0) {
			String[] tokens = message.split("#\\^#");
			err.setCode((null != tokens[0] ? ("LO19".equals(tokens[0].trim()) ? "LO83": tokens[0].trim()): ""));
			err.setMessage(tokens[1]);
		} else {
			err.setCode("LO83");
			err.setMessage("OIM doesn't return a error message though the operation failed.");
		}
		return err;
	}

	private OIDMErrorDTO extractErrorPWCalls(OIDMErrorDTO err, String message,Map<String, Object> muleProps, Object[] params) {
		logger.error("Method: " + muleProps.get("operation") + " Input: "+ Arrays.toString(params) + " Error: " + message);
		if (null != message && message.length() > 0) {
			String[] tokens = message.split("#\\^#");
			err.setCode((null != tokens[0] ? tokens[0].trim() : ""));
			err.setMessage(tokens[1]);
		} else {
			err.setCode("LO83");
			err.setMessage("OIM doesn't return a error message though the operation failed.");
		}
		return err;
	}

	public Comparator<StaffDTO> sortByFamilyGivenNames() {
		return new Comparator<StaffDTO>() {
			public int compare(StaffDTO dto1, StaffDTO dto2) {
				if (null == dto1.getFamilyName()) {
					dto1.setFamilyName("");
				}
				if (null == dto2.getFamilyName()) {
					dto2.setFamilyName("");
				}
				int comp = dto1.getFamilyName().compareTo(dto2.getFamilyName());
				if (comp == 0) {
					if (null != dto1.getFirstName()
							&& null != dto2.getFirstName()) {
						comp = dto1.getFirstName().compareTo(
								dto2.getFirstName());
					}
				}
				if (comp == 0) {
					if (null != dto1.getSecondName()
							&& null != dto2.getSecondName()) {
						comp = dto1.getSecondName().compareTo(
								dto2.getSecondName());
					}
				}
				return comp;
			}
		};
	}

	private boolean isDigestValid(String[] pParams, String pDigest, String pCheckDigest) {
		
		try {
			
			if (!"PROD".equals(MuleContextListener.env) && StringUtils.isNotBlank(pCheckDigest) && "N".equals(pCheckDigest)) {
				return true;
			}
			
			System.out.println("XXXX: "
					+ new OIMUtil().getEncodedDigest(pParams)
					+ " Portal digest: " + pDigest);
			if (null != pDigest
					&& pDigest.equals(new OIMUtil().getEncodedDigest(pParams))) {
				return true;
			} else {
				return false;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}

	}
	
	public static void initPasswords() throws Exception {
		try {
			Properties passwords = new Properties();
			passwords.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("passwords.properties"));
			ClientPasswordCallback.init(passwords);
			System.out.println("OIM Passwords initialised.");
		} catch (Exception xe) {
		//	System.out.println("Passwords initialisation failed" + xe);
			throw new Exception("Passwords initialisation failed", xe);
		}
	}

	/**
	 * @GET
	 * @Path("/roles/usersInRole")
	 * @Produces(MediaType.APPLICATION_JSON) public UsersInRoleResponse
	 *                                       usersInRole(@QueryParam("adminId")
	 *                                       String
	 *                                       adminId,@QueryParam("roleName")
	 *                                       String roleName) { OidmPayload
	 *                                       payload = new
	 *                                       OidmPayload(OP.USER_IN_ROLE);
	 *                                       payload.setRestRequestObject(new
	 *                                       UsersInRoleRequest(adminId,
	 *                                       roleName));
	 *                                       payload.setRestResponseObject(new
	 *                                       UsersInRoleResponse()); payload =
	 *                                       start(payload);
	 * 
	 *                                       UsersInRoleResponse response =
	 *                                       (UsersInRoleResponse)
	 *                                       payload.getRestResponseObject();
	 *                                       setStatus(response, payload);
	 *                                       return response; }
	 * @POST
	 * @Path("/pwd/modifyUserExpiryDate")
	 * @Produces({ MediaType.APPLICATION_JSON })
	 * @Consumes({ MediaType.APPLICATION_JSON }) public GenericResponse
	 *             modifyUserExpiryDate(ModifyUserExpiryDateRequest request) {
	 *             OidmPayload payload = new
	 *             OidmPayload(OP.MODIFY_USER_EXPIRY_DATE);
	 *             payload.setRestRequestObject(request);
	 *             payload.setRestResponseObject(new GenericResponse()); payload
	 *             = start(payload);
	 * 
	 *             GenericResponse response = (GenericResponse)
	 *             payload.getRestResponseObject(); setStatus(response,
	 *             payload); return response; }
	 * @GET
	 * @Path("/roles/populateRoles")
	 * @Produces(MediaType.APPLICATION_JSON) public String
	 *                                       populateRoles(@QueryParam("userId")
	 *                                       String userId) throws
	 *                                       RESTApplicationException {
	 *                                       OIDMErrorDTO err = new
	 *                                       OIDMErrorDTO(); OIDMResultDTO
	 *                                       result = new OIDMResultDTO();
	 * 
	 *                                       Map<String, String> muleProps = new
	 *                                       HashMap<String, String>();
	 *                                       muleProps.put("operation",
	 *                                       "getBusinessRoles"); Object[]
	 *                                       params = new Object[] {};
	 * 
	 * 
	 *                                       GetBusinessRolesResp response =
	 *                                       (GetBusinessRolesResp) callOIDM (
	 *                                       muleProps, params);
	 * 
	 *                                       try { if (response.isSuccess()) {
	 *                                       result.setResult(response.
	 *                                       getBusinessRoles()); } else { err =
	 *                                       extractError(err,
	 *                                       response.getMessage()); }
	 * 
	 *                                       result.setError(err);
	 * 
	 * 
	 *                                       return
	 *                                       mapper.writeValueAsString(result);
	 *                                       } catch (Exception ex) {
	 *                                       ex.printStackTrace(); throw new
	 *                                       RESTApplicationException
	 *                                       ("Failed - " +
	 *                                       muleProps.get("operation"), ex); }
	 *                                       }
	 */

}
