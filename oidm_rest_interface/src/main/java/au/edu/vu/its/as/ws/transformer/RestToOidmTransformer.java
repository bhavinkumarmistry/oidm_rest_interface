package au.edu.vu.its.as.ws.transformer;

import au.edu.vu.its.as.presn.dto.ConditionalDescriptorDTO;
import au.edu.vu.its.as.presn.dto.DescriptorDTO;
import au.edu.vu.its.as.presn.dto.oidm.ForgotPasswordDTO;
import au.edu.vu.its.as.presn.dto.oidm.StaffDTO;
import au.edu.vu.its.as.ws.io.AddUserRequest;
import au.edu.vu.its.as.ws.io.CheckUserAnswersRequest;
import au.edu.vu.its.as.ws.io.ModifyUserRequest;
import au.edu.vu.its.as.ws.io.ModifyUserRoleRequest;

import java.util.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.vu.ws.GetSecretQuestionBankResp;
import com.vu.ws.GetSecretQuestionParamsResp;
import com.vu.ws.GetUsersSecretQuestionsResp;
import com.vu.ws.Modifyuserbody;

public class RestToOidmTransformer {

	private static final String DELIMITER = "\\|\\|";

	public static List<String> createAnswersList(List<DescriptorDTO> pAnswers) {

		DescriptorDTO[] arr = pAnswers.toArray(new DescriptorDTO[0]);

		List<String> answers = new ArrayList<String>();

		for (int i = 0; i < arr.length; i++) {
			String reply = arr[i].getDescription();
			answers.add(arr[i].getId() + "||" + ((null == reply) ? "" : reply));
		}

		return answers;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static Object[] transformAddUser(AddUserRequest pRequest) throws Exception {

		StaffDTO staff = pRequest.getStaff();
		Object[] params = new Object[] { pRequest.getAdminId(),
				OidmToRestTransformer.LOGIN_PREFIX + staff.getManagerId(), staff.getFirstName(),
				staff.getFamilyName(), staff.getSecondName(),
				toGregorianCalendar(staff.getEndOfContract()),
				staff.getCostCentre(), staff.getCostGroup(),
				staff.getCampusIdHR(), staff.getExtension(),
				staff.getPersonalEmail(), staff.getDepartmentIdHR() };

		return params;

	}

	
	public static Object[] transformModifyUser(ModifyUserRequest pRequest) throws Exception {

		StaffDTO staff = pRequest.getStaff();
		
		Modifyuserbody req = new Modifyuserbody();
		
		req.setAdminId(pRequest.getAdminId());
		req.setUserID(OidmToRestTransformer.LOGIN_PREFIX + staff.getVuId());
		req.setManagerId(OidmToRestTransformer.LOGIN_PREFIX + staff.getManagerId());
		req.setFirstName(staff.getFirstName());
		req.setLastName(staff.getFamilyName());
		req.setMiddleName(staff.getSecondName());
		req.setEndOfContract(toGregorianCalendar(staff.getEndOfContract()));
		req.setCostCentresHR(staff.getCostCentre());
		req.setCostGroup(staff.getCostGroup());
		req.setCampusIdHR(staff.getCampusIdHR());
		req.setExtension(staff.getExtension());
		req.setPersonalEmail(staff.getPersonalEmail());
		req.setOrgUnitIdHR(staff.getDepartmentIdHR() );
		
		Object[] params = new Object[] {req};
		
		/*
		Object[] params = new Object[] { pRequest.getAdminId(),
				staff.getLoginId(), staff.getManagerId(), staff.getFirstName(),
				staff.getFamilyName(), staff.getSecondName(),
				toGregorianCalendar(staff.getEndOfContract()),
				staff.getCostCentre(), staff.getCostGroup(),
				staff.getCampusIdHR(), staff.getExtension(),
				staff.getPersonalEmail(), staff.getDepartmentIdHR() };
        */
		
		return params;

	}

	
	public static Object[] transformModifyUserRole(
			ModifyUserRoleRequest pRequest) {
		Object[] params = new Object[] { pRequest.getAdminId(),
				pRequest.getUserId(), pRequest.getRole(), pRequest.getDateTo(),
			   pRequest.getDateFrom(), ("Y".equals(pRequest.getDefault())?true:false)
				 };

		return params;

	}
	
	public static Object[] transformCheckUserAnswers(
			CheckUserAnswersRequest pRequest) {
		Object[] params = new Object[] { pRequest.getUserId(), createAnswersList(pRequest.getDescriptors())};

		return params;

	}
	

	
	private static XMLGregorianCalendar toGregorianCalendar(Date pDate)
			throws DatatypeConfigurationException {
		{
			DatatypeFactory df = DatatypeFactory.newInstance();
			if (pDate == null) {
				return null;
			} else {
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTimeInMillis(pDate.getTime());
				return df.newXMLGregorianCalendar(gc);
			}
		}
	}

	

	
	
	

}
