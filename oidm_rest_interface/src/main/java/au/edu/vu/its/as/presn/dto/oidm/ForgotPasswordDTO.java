/*
*
* File Name                      :  ForgotPasswordDTO.java
* Creation/Modification History  :
*
*  08-Aug-11  Vladimir Gleiberman  Created
*
*/


package au.edu.vu.its.as.presn.dto.oidm;


/**
 * Copyright (C) 2011 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * This class is used to transfer information related to forgotten password
 *
 * @author   : Vladimir Gleiberman
 * @version  : 11.5
 */

public class ForgotPasswordDTO implements java.io.Serializable { 
   
   private String[] secretQuestions;
   private String mobilePhone;
   private String email;
      

   /**
    * Returns User secret questions
    *
    * @return <b>String[]</b> User secret questions
   */
   public String[] getSecretQuestions() {
      return secretQuestions;
   }

   
   /**
    * Returns User mobile phone number
    *
    * @return <b>String</b> user mobile phone number
   */
   public String getMobilePhone() {
      return mobilePhone;
   }

   
   /**
    * Returns User email
    *
    * @return <b>email</b> User email
   */
   public String getEmail() {
      return email;
   }
   

   /**
    * Sets the value of secret questions
    *
    * @param  <b>psecretQuestions</b> User secret questions
   */
   public void setSecretQuestions(String[] pSecretQuestions) {
      secretQuestions = pSecretQuestions;
   }

   
   /**
    * Sets the value of mobile phone number
    *
    * @param  <b>pmobilePhone</b> User mobile phone number
   */
   public void setMobilePhone(String pMobilePhone) {
      mobilePhone = pMobilePhone;
   }

   
   /**
    * Sets the value of mail End String
    *
    * @param  <b>pemail</b> User mail End String
   */
   public void setEmail(String pEmail) {
      email = pEmail;
   }
   

   /**
    * Returns the string representation of this Class.
    *
    * @return <b>String</b> printable version of an instance
    */
   public String toString() {

      StringBuilder sb = new StringBuilder("[ForgotPasswordDTO]: ");
      sb.append("mobile=" + getMobilePhone() + " email=" + getEmail() + "\n");
      String[] sq = getSecretQuestions();
      sb.append("Secret Questions " + sq.length + ":");
      for (String q : sq) {
         sb.append(q + "/");
      }

      return sb.toString();
   }
}
