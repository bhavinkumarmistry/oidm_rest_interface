package au.edu.vu.its.as.ws.io;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import au.edu.vu.its.as.ws.io.AbstractRequest;
import au.edu.vu.its.as.ws.io.Verifiable;
import au.edu.vu.its.as.presn.dto.DescriptorDTO;


@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetUserSecretAnswersRequest extends AbstractRequest {

	
	@XmlElement(name = "userId")
	private String userId;
	
	@XmlElement(name = "mobile")
	private String mobile;
	
	@XmlElement(name = "email")
	private String email;
	
	@XmlElement(name = "answers")
	private List<DescriptorDTO> answers;
	
	@XmlElement(name = "digest")
	private String digest;

	
	public SetUserSecretAnswersRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<DescriptorDTO> getAnswers() {
		return answers;
	}

	public void setAnswers(List<DescriptorDTO> answers) {
		this.answers = answers;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

		
}
