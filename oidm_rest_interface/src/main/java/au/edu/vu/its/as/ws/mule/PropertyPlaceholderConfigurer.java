/*
*
* File Name                      :  PropertyPlaceholderConfigurer.java
* Creation/Modification History  :
*
*   27-Aug-2012  Vladimir GLEIBERMAN  Created
*
*/

package au.edu.vu.its.as.ws.mule;

import au.edu.vu.its.as.util.CryptoUtil;
import au.edu.vu.its.as.util.Log;


/**
 * Copyright (C) 2012 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * Spring placeholders loader with Decryption
 *
 * @author   : Vladimir GLEIBERMAN
 * @version  : 12.6
 */
public class PropertyPlaceholderConfigurer extends
   org.springframework.beans.factory.config.PropertyPlaceholderConfigurer {


   /**
    * Checks if a property is encrypted and decrypts it.
    *
    * @param  <b>originalValue</b> original value of a property
    * @return <b>String</b> decrypted (if required) value of the property
    */
   @Override
   protected String convertPropertyValue(final String originalValue) {

      String value = originalValue.trim();
      if (value.startsWith("ENC(") && value.endsWith(")")) {
         if (null != CryptoUtil.VU_KEY) {
            try {
               value = CryptoUtil.decodeSymmetricalPrintable(
                          CryptoUtil.VU_KEY, value.substring(4,value.length()));
            } catch (Exception xe) {
               String m = "Placeholder decrypt failed for " + value;
               System.out.println(m + ": " + xe);
               Log.error(m, xe);
            }
         } else {
            String m = "Secret key not available to decrypt " + value;
            System.out.println(m);
            Log.error(m);
         }
      }

      return value;
   }
}
