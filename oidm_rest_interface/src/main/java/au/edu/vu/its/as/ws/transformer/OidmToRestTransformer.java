package au.edu.vu.its.as.ws.transformer;

import au.edu.vu.its.as.presn.dto.ConditionalDescriptorDTO;
import au.edu.vu.its.as.presn.dto.DescriptorDTO;
import au.edu.vu.its.as.presn.dto.oidm.*;
import au.edu.vu.its.as.ws.io.AddUserRequest;

import java.text.SimpleDateFormat;
import java.util.*;


import com.vu.ws.AddUserResp;
import com.vu.ws.GetSecretQuestionBankResp;
import com.vu.ws.GetSecretQuestionParamsResp;
import com.vu.ws.GetUsersSecretQuestionsResp;
import com.vu.ws.UserRole;

public class OidmToRestTransformer {

	private static final String DELIMITER = "\\|\\|";
	private static final SimpleDateFormat DATE_FORMATTER =
		      new SimpleDateFormat("dd/MM/yyyy");
	protected static final SimpleDateFormat ROLE_DATE_FORMATTER = new SimpleDateFormat(
			"dd/MM/yyyy");
	public static final String LOGIN_PREFIX = "e";

	public static ConditionalDescriptorDTO[] transformSecretQuestionBank(
			GetSecretQuestionBankResp response) {
		List<ConditionalDescriptorDTO> dtos = new ArrayList<ConditionalDescriptorDTO>();

		for (String question : response.getSecretQuestionsBank()) {
			String[] tokens = question.split(DELIMITER);
			boolean[] active = new boolean[2];
			active[0] = "Y".equals(tokens[1]) ? true : false;
			active[1] = "Y".equals(tokens[2]) ? true : false;
			dtos.add(new ConditionalDescriptorDTO(tokens[0], tokens[3], active));
		}
		return dtos.toArray(new ConditionalDescriptorDTO[0]);
	}

	public static DescriptorDTO[] transformSecretQuestionParams(
			GetSecretQuestionParamsResp response) {
		List<DescriptorDTO> dtos = new ArrayList<DescriptorDTO>();

		for (String param : response.getQuestionBankPolicy()) {
			DescriptorDTO dto = new DescriptorDTO();
			String[] tokens = param.split(DELIMITER);
			dto.setId(tokens[0]);
			dto.setDescription(tokens[1]);
			dtos.add(dto);
		}
		return dtos.toArray(new DescriptorDTO[0]);
	}

	public static ForgotPasswordDTO transformUserSecretQuestions(
			GetUsersSecretQuestionsResp response) {
		
		ForgotPasswordDTO fpDTO = new ForgotPasswordDTO();
		fpDTO.setEmail(response.getPersonalEmail());
		fpDTO.setMobilePhone(response.getMobile());
        if (null != response.getUserSecretQuestions()) {
        	fpDTO.setSecretQuestions(response.getUserSecretQuestions().toArray(new String[0]));
        }
		
		return fpDTO;
	}
	
	 /**
	    * Extract Roles information from Responce
	    *
	    * @param  <b>pResponse</b> IDM SPML Response
	    * @return <b>RoleDTO[]</b> Roles
	    */
	   public static RoleDTO[] extractRoles(List<UserRole> userRoles) throws Exception {

	      if (null == userRoles) {
	         return new RoleDTO[0];
	      }

	      List<RoleDTO> roles = new ArrayList<RoleDTO> ();
	      
	      for (UserRole role : userRoles) {
	    	 RoleDTO dto = new RoleDTO();
	    	 dto.setRole(role.getRoleName());

	    	 dto.setDateFrom(DATE_FORMATTER.parse(role.getAssignedFrom()));
	    	 dto.setDateTo(DATE_FORMATTER.parse(role.getAssignedTo()));
	      }
	    
	      return roles.toArray(new RoleDTO[0]);
	   }



	public static StaffDTO transformAddUser(AddUserRequest pRequest,
			AddUserResp pResponse) throws Exception {
		StaffDTO staff = pRequest.getStaff();
		staff.setLoginId(pResponse.getNewStaffId());
		staff.setVuId(null != pResponse.getNewStaffId()?pResponse.getNewStaffId().replace(LOGIN_PREFIX, ""):"");
		staff.setEmail(pResponse.getEmail());
		
		return staff;
	}

}
