/*
 *
 * File Name                      :  StaffDTO.java
 * Creation/Modification History  :
 *
 * 15-Feb-10  Andrew RUAN  Created
 * 19-Apr-10  Andrew RUAN  Removed roles
 * 22-Jul-13  Chelladurai KANAGARATNAM ManagerName added
 *
 */
package au.edu.vu.its.as.presn.dto.oidm;

/**
 * Copyright (C) 2010 Victoria University, Melbourne, VIC AUS All Rights
 * Reserved.
 * 
 * This class used to transfer information b/w presentation/business layers
 * 
 * @author : Andrew RUAN
 * @Version : 10.1
 */

import java.math.BigDecimal;
import java.util.Date;

public class StaffDTO extends PersonDTO implements java.io.Serializable {

   private String     faxNumber;
   private String     telephoneNumber;
   private String     mobile;
   private String     roomNumber;
   private BigDecimal fte;
   private String     monitorNumber;
   private String     barcodeNumber;
   private Integer    leaveHours;
   private String     campusIdHR;
   private String     campusIdSMS;
   private Date       endOfContract;
   private String     costGroup;
   private String     extension;
   private String     positionType;
   private String     positionTitle;
   private String     secondName;
   private Date       terminationDate;
   private String     facultyIdHR;
   private String     departmentIdHR;
   private String     facultyIdSMS;
   private String     departmentIdSMS;
   private String     managerId;
   private String     managerName;

   /**
    * Returns faxNumber value
    * 
    * @return <b>faxNumber</b> faxNumber value
    */
   public String getFaxNumber() {
      return faxNumber;
   }


   /**
    * Sets the value of faxNumber
    * 
    * @param <b>pFaxNumber</b> faxNumber value
    */
   public void setFaxNumber(String pFaxNumber) {
      faxNumber = pFaxNumber;
   }
   

   /**
    * Returns telephoneNumber value
    * 
    * @return <b>telephoneNumber</b> telephoneNumber value
    */
   public String getTelephoneNumber() {
      return telephoneNumber;
   }


   /**
    * Sets the value of telephoneNumber
    * 
    * @param <b>pTelephoneNumber</b> telephoneNumber value
    */
   public void setTelephoneNumber(String pTelephoneNumber) {
      telephoneNumber = pTelephoneNumber;
   }


   /**
    * Returns mobile value
    * 
    * @return <b>mobile</b> mobile value
    */
   public String getMobile() {
      return mobile;
   }


   /**
    * Sets the value of mobile
    * 
    * @param <b>pMobile</b> mobile value
    */
   public void setMobile(String pMobile) {
      mobile = pMobile;
   }


   /**
    * Returns roomNumber value
    * 
    * @return <b>roomNumber</b> roomNumber value
    */
   public String getRoomNumber() {
      return roomNumber;
   }


   /**
    * Sets the value of roomNumber
    * 
    * @param <b>pRoomNumber</b> roomNumber value
    */
   public void setRoomNumber(String pRoomNumber) {
      roomNumber = pRoomNumber;
   }


   /**
    * Returns fte value
    * 
    * @return <b>fte</b> fte value
    */
   public BigDecimal getFte() {
      return fte;
   }


   /**
    * Sets the value of fte
    * 
    * @param <b>pFte</b> fte value
    */
   public void setFte(BigDecimal pFte) {
      fte = pFte;
   }


   /**
    * Returns monitorNumber value
    * 
    * @return <b>monitorNumber</b> monitorNumber value
    */
   public String getMonitorNumber() {
      return monitorNumber;
   }


   /**
    * Sets the value of monitorNumber
    * 
    * @param <b>pMonitorNumber</b> monitorNumber value
    */
   public void setMonitorNumber(String pMonitorNumber) {
      monitorNumber = pMonitorNumber;
   }


   /**
    * Returns barcodeNumber value
    * 
    * @return <b>barcodeNumber</b> barcodeNumber value
    */
   public String getBarcodeNumber() {
      return barcodeNumber;
   }


   /**
    * Sets the value of barcodeNumber
    * 
    * @param <b>pBarcodeNumber</b> barcodeNumber value
    */
   public void setBarcodeNumber(String pBarcodeNumber) {
      barcodeNumber = pBarcodeNumber;
   }


   /**
    * Returns leaveHours value
    * 
    * @return <b>leaveHours</b> leaveHours value
    */
   public Integer getLeaveHours() {
      return leaveHours;
   }


   /**
    * Sets the value of leaveHours
    * 
    * @param <b>pLeaveHours</b> leaveHours value
    */
   public void setLeaveHours(Integer pLeaveHours) {
      leaveHours = pLeaveHours;
   }


   /**
    * Returns campusIdHR value
    * 
    * @return <b>campusIdHR</b> campusIdHR value
    */
   public String getCampusIdHR() {
      return campusIdHR;
   }


   /**
    * Sets the value of campusIdHR
    * 
    * @param <b>pCampusIdHR</b> campusIdHR value
    */
   public void setCampusIdHR(String pCampusIdHR) {
      campusIdHR = pCampusIdHR;
   }


   /**
    * Returns campusIdSMS value
    * 
    * @return <b>campusIdSMS</b> campusIdSMS value
    */
   public String getCampusIdSMS() {
      return campusIdSMS;
   }


   /**
    * Sets the value of campusIdSMS
    * 
    * @param <b>pCampusIdSMS</b> campusIdSMS value
    */
   public void setCampusIdSMS(String pCampusIdSMS) {
      campusIdSMS = pCampusIdSMS;
   }

   /**
    * Returns endOfContract value
    * 
    * @return <b>endOfContract</b> endOfContract value
    */
   public Date getEndOfContract() {
      return endOfContract;
   }


   /**
    * Sets the value of endOfContract
    * 
    * @param <b>pEndOfContract</b> endOfContract value
    */
   public void setEndOfContract(Date pEndOfContract) {
      endOfContract = pEndOfContract;
   }


   /**
    * Returns costGroup value
    * 
    * @return <b>costGroup</b> costGroup value
    */
   public String getCostGroup() {
      return costGroup;
   }


   /**
    * Sets the value of costGroup
    * 
    * @param <b>pCostGroup</b> costGroup value
    */
   public void setCostGroup(String pCostGroup) {
      costGroup = pCostGroup;
   }


   /**
    * Returns extension value
    * 
    * @return <b>extension</b> extension value
    */
   public String getExtension() {
      return extension;
   }


   /**
    * Sets the value of extension
    * 
    * @param <b>pExtension</b> extension value
    */
   public void setExtension(String pExtension) {
      extension = pExtension;
   }


   /**
    * Returns positionType value
    * 
    * @return <b>positionType</b> positionType value
    */
   public String getPositionType() {
      return positionType;
   }


   /**
    * Sets the value of positionType
    * 
    * @param <b>pPositionType</b> positionType value
    */
   public void setPositionType(String pPositionType) {
      positionType = pPositionType;
   }


   /**
    * Returns positionTitle value
    * 
    * @return <b>positionTitle</b> positionTitle value
    */
   public String getPositionTitle() {
      return positionTitle;
   }


   /**
    * Sets the value of positionTitle
    * 
    * @param <b>pPositionTitle</b> positionTitle value
    */
   public void setPositionTitle(String pPositionTitle) {
      positionTitle = pPositionTitle;
   }


   /**
    * Returns secondName value
    * 
    * @return <b>secondName</b> secondName value
    */
   public String getSecondName() {
      return secondName;
   }


   /**
    * Sets the value of secondName
    * 
    * @param <b>pSecondName</b> secondName value
    */
   public void setSecondName(String pSecondName) {
      secondName = pSecondName;
   }


   /**
    * Returns terminationDate value
    * 
    * @return <b>terminationDate</b> terminationDate value
    */
   public Date getTerminationDate() {
      return terminationDate;
   }


   /**
    * Sets the value of terminationDate
    * 
    * @param <b>pTerminationDate</b> terminationDate value
    */
   public void setTerminationDate(Date pTerminationDate) {
      terminationDate = pTerminationDate;
   }


   /**
    * Returns facultyIdHR value
    * 
    * @return <b>facultyIdHR</b> facultyIdHR value
    */
   public String getFacultyIdHR() {
      return facultyIdHR;
   }


   /**
    * Sets the value of facultyIdHR
    * 
    * @param <b>pFacultyIdHR</b> facultyIdHR value
    */
   public void setFacultyIdHR(String pFacultyIdHR) {
      facultyIdHR = pFacultyIdHR;
   }


   /**
    * Returns departmentIdHR value
    * 
    * @return <b>departmentIdHR</b> departmentIdHR value
    */
   public String getDepartmentIdHR() {
      return departmentIdHR;
   }


   /**
    * Sets the value of departmentIdHR
    * 
    * @param <b>pDepartmentIdHR</b> departmentIdHR value
    */
   public void setDepartmentIdHR(String pDepartmentIdHR) {
      departmentIdHR = pDepartmentIdHR;
   }



   /**
    * Returns facultyIdSMS value
    * 
    * @return <b>facultyIdSMS</b> facultyIdSMS value
    */
   public String getFacultyIdSMS() {
      return facultyIdSMS;
   }


   /**
    * Sets the value of facultyIdSMS
    * 
    * @param <b>pFacultyIdSMS</b> facultyIdSMS value
    */
   public void setFacultyIdSMS(String pFacultyIdSMS) {
      facultyIdSMS = pFacultyIdSMS;
   }


   /**
    * Returns departmentIdSMS value
    * 
    * @return <b>departmentIdSMS</b> departmentIdSMS value
    */
   public String getDepartmentIdSMS() {
      return departmentIdSMS;
   }


   /**
    * Sets the value of departmentIdSMS
    * 
    * @param <b>pDepartmentIdSMS</b> departmentIdSMS value
    */
   public void setDepartmentIdSMS(String pDepartmentIdSMS) {
      departmentIdSMS = pDepartmentIdSMS;
   }
   
   
   /**
    * Returns managerId value
    * 
    * @return <b>managerId</b> managerId value
    */
   public String getManagerId() {
      return managerId;
   }


   /**
    * Sets the value of managerId
    * 
    * @param <b>pManagerId</b> managerId value
    */
   public void setManagerId(String pManagerId) {
      managerId = pManagerId;
   }
   
   /**
    * Returns managerName value
    * 
    * @return <b>managerName</b> managerName value
    */
   public String getManagerName() {
      return managerName;
   }


   /**
    * Sets the value of managerName
    * 
    * @param <b>pManagerName</b> managerName value
    */
   public void setManagerName(String pManagerName) {
      managerName = pManagerName;
   }

	
   /**
    * Returns String
    * 
    * @return <b>String</b> String
    */
   public String toString() {
      StringBuilder str = new StringBuilder();
      str.append("Staff[");
      str.append("faxNumber = ").append(faxNumber);
      str.append(", telephoneNumber = ").append(telephoneNumber);
      str.append(", mobile = ").append(mobile);
      str.append(", roomNumber = ").append(roomNumber);
      str.append(", fte = ").append(fte);
      str.append(", monitorNumber = ").append(monitorNumber);
      str.append(", barcodeNumber = ").append(barcodeNumber);
      str.append(", leaveHours = ").append(leaveHours);
      str.append(", campusIdHR = ").append(campusIdHR);
      str.append(", campusIdSMS = ").append(campusIdSMS);
      str.append(", endOfContract = ").append(endOfContract);
      str.append(", costGroup = ").append(costGroup);
      str.append(", extension = ").append(extension);
      str.append(", positionType = ").append(positionType);
      str.append(", positionTitle = ").append(positionTitle);
      str.append(", secondName = ").append(secondName);
      str.append(", terminationDate = ").append(terminationDate);
      str.append(", facultyIdHR = ").append(facultyIdHR);
      str.append(", departmentIdHR = ").append(departmentIdHR);
      str.append(", facultyIdSMS = ").append(facultyIdSMS);
      str.append(", departmentIdSMS = ").append(departmentIdSMS);
      str.append(", managerId = ").append(managerId);
      str.append(", managerName = ").append(managerName);
      str.append("]");
      return super.toString() + "\n      " + str.toString();
   }





}
