package au.edu.vu.its.as.ws.io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import au.edu.vu.its.as.ws.io.*;;



@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class ModifyUserRoleRequest extends AbstractRequest  {
	
	
	@XmlElement(name = "adminId")
	private String adminId;
	
	@XmlElement(name = "userId")
	private String userId;
	
	@XmlElement(name = "role")
	private String role;
	
	@XmlElement(name = "dateFrom")
	private String dateFrom;
	
	@XmlElement(name = "dateTo")
	private String dateTo;
	
	@XmlElement(name = "default")
	private String default1;

	@XmlElement(name = "digest")
	private String digest;
	
	public ModifyUserRoleRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}

	public String getDateTo() {
		return dateTo;
	}

	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}

	public String getDefault() {
		return default1;
	}

	public void setDefault(String default1) {
		this.default1 = default1;
	}




	public String getDigest() {
		// TODO Auto-generated method stub
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

}
