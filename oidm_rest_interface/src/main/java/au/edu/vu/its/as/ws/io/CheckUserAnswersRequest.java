package au.edu.vu.its.as.ws.io;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import au.edu.vu.its.as.ws.io.AbstractRequest;
import au.edu.vu.its.as.presn.dto.DescriptorDTO;

@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class CheckUserAnswersRequest extends AbstractRequest {

	@XmlElement(name = "userId")
	private String userId;
	
	@XmlElement(name = "descriptors")
	private List<DescriptorDTO> descriptors;
	
	@XmlElement(name = "digest")
	private String digest;
	
	public CheckUserAnswersRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<DescriptorDTO> getDescriptors() {
		return descriptors;
	}

	public void setDescriptors(List<DescriptorDTO> descriptors) {
		this.descriptors = descriptors;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	
	
}
