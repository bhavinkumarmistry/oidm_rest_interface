package au.edu.vu.its.as.ws.ldap;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.naming.directory.Attributes;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import au.edu.vu.its.as.bus.ApplicationException;
import au.edu.vu.its.as.ds.LdapClient;
import au.edu.vu.its.as.presn.dto.oidm.AuthenticationDTO;
import au.edu.vu.its.as.presn.dto.oidm.RoleDTO;
import au.edu.vu.its.as.presn.dto.oidm.StaffDTO;
import au.edu.vu.its.as.ws.oidm.OIDMResource;


public class LDAPData {
	
	protected static final SimpleDateFormat ROLE_DATE_FORMATTER = new SimpleDateFormat(
			"dd/MM/yyyy");
	private static Log logger = LogFactory.getLog(LDAPData.class);
	
	/**
	 * Get staff info per LoginId
	 * 
	 * @param <b>pLoginId</b> Staff Login Id
	 * @return <b>StaffDTO</b> Staff LDAP Attributes
	 * @throw <b>ApplicationException</b> if DS problems
	 */

	public StaffDTO getStaffInfo(String pLoginId, Properties edsProps) throws ApplicationException {
		SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat( "yyyyMMdd");

		StaffDTO staff = new StaffDTO();
		if (null == pLoginId) {
			return staff;
		}

		LdapClient lc = new LdapClient();
		try {
			Attributes attrs = lc.getPersonInfo(edsProps.getProperty("host"), edsProps.getProperty("port"), edsProps.getProperty("admin"), edsProps.getProperty("password"),
					edsProps.getProperty("org"), pLoginId);
			if (null == attrs) { // possibly Student Id passed
				logger.error("No EDS record found for loginid " + pLoginId + " .");
				return staff;
			}
			staff.setLoginId(lc.getAttributeValue(attrs, "uid"));
			staff.setVuId(lc.getAttributeValue(attrs, "employeeNumber"));
			staff.setFirstName(lc.getAttributeValue(attrs, "givenName"));
			staff.setFamilyName(lc.getAttributeValue(attrs, "sn"));
			staff.setSecondName(lc.getAttributeValue(attrs, "vuMiddleName"));
			staff.setCampusIdSMS(lc.getAttributeValue(attrs, "vuCampusIdSMS"));
			staff.setCampusIdHR(lc.getAttributeValue(attrs, "vuCampusIdHR"));
			staff.setFacultyIdHR(lc.getAttributeValue(attrs, "vuFacultyHR"));
			staff.setFacultyIdSMS(lc.getAttributeValue(attrs, "vuFacultySMS"));
			staff.setDepartmentIdHR(lc.getAttributeValue(attrs,
					"vuDepartmentHR"));
			staff.setDepartmentIdSMS(lc.getAttributeValue(attrs,
					"vuDepartmentSMS"));
			staff.setCostGroup(lc.getAttributeValue(attrs, "vuCostGroup"));
			staff.setExtension(lc.getAttributeValue(attrs, "vuExtension"));
			staff.setPositionType(lc.getAttributeValue(attrs, "vuPositionType"));
			staff.setFaxNumber(lc.getAttributeValue(attrs,
					"facsimileTelephoneNumber"));
			staff.setEmail(lc.getAttributeValue(attrs, "mail"));
			staff.setTitle(lc.getAttributeValue(attrs, "personalTitle"));
			staff.setPositionTitle(lc.getAttributeValue(attrs, "title"));
			staff.setLibPatronCode(lc.getAttributeValue(attrs,
					"vuLibPatronCode"));
			staff.setAuthoritativeSource(lc.getAttributeValue(attrs,
					"vuAuthoritativeSource"));
			staff.setTelephoneNumber(lc.getAttributeValue(attrs,
					"telephoneNumber"));
			staff.setRoomNumber(lc.getAttributeValue(attrs, "roomNumber"));
			staff.setMobile(lc.getAttributeValue(attrs, "mobile"));
			staff.setMonitorNumber(lc.getAttributeValue(attrs,
					"vuMonitorNumber"));
			staff.setBarcodeNumber(lc.getAttributeValue(attrs,
					"vuBarcodeNumber"));
			staff.setManagerId(lc.getAttributeValue(attrs, "vuManagerId"));
			staff.setCostCentre(lc.getAttributeValue(attrs, "vuCostCentres"));			
			String t = lc.getAttributeValue(attrs, "vuEndOfContract");
			if (null != t) {
				staff.setEndOfContract(DATE_FORMATTER.parse(t.substring(0, 8)));
			}
			t = lc.getAttributeValue(attrs, "vuExpiryDate");
			if (null != t) {
				staff.setExpiryDate(DATE_FORMATTER.parse(t.substring(0, 8)));
			}
			t = lc.getAttributeValue(attrs, "vuTerminationDate");
			if (null != t) {
				staff.setTerminationDate(DATE_FORMATTER.parse(t.substring(0, 8)));
			}
			t = lc.getAttributeValue(attrs, "vuLeaveHours");
			if (null != t) {
				staff.setLeaveHours(Integer.parseInt(t));
			}
			t = lc.getAttributeValue(attrs, "vuFte");
			if (null != t) {
				staff.setFte(new BigDecimal(t));
			}
			
			AuthenticationDTO authInfo = new AuthenticationDTO();
			authInfo.setDefaultRole(lc.getAttributeValue(attrs,
					"vuDefaultRole"));
			authInfo.setRoles(extractRoles(lc.getAttributeValue(attrs,
					"vuRoleLists")));
			staff.setAuthenticationInfo(authInfo);

			// lc.disconnect();
		} catch (Exception ex) {
			throw new ApplicationException(ex.getMessage(), ex);
		}

		return staff;
	}
	
	 /**
	 * Extract Roles information from Responce
	 * 
	 * @param <b>pResponse</b> IDM SPML Response
	 * @return <b>RoleDTO[]</b> Roles
	 */
	public RoleDTO[] extractRoles(String pRoleList) {

		if (null == pRoleList) {
			return new RoleDTO[0];
		}

		String[] rs = pRoleList.split(",");
		int n = rs.length;
		RoleDTO[] roles = new RoleDTO[n];
		for (int i = 0; i < n; i++) {
			roles[i] = new RoleDTO();
			String[] oneRole = rs[i].split(";");
			roles[i].setRole(oneRole[0]);
			int m = oneRole.length;
			if (m > 1 && null != oneRole[1]) {
				try {
					roles[i].setDateFrom(ROLE_DATE_FORMATTER.parse(oneRole[1]));
				} catch (Exception ignore) {
				}
			}
			if (m > 2 && null != oneRole[2]) {
				try {
					roles[i].setDateTo(ROLE_DATE_FORMATTER.parse(oneRole[2]));
				} catch (Exception ignore) {
				}
			}
		}

		return roles;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
