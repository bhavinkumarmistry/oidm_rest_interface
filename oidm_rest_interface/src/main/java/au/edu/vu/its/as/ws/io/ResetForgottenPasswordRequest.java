package au.edu.vu.its.as.ws.io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import au.edu.vu.its.as.ws.io.AbstractRequest;


@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResetForgottenPasswordRequest  extends AbstractRequest {

	@XmlElement(name = "userId")
	private String userId;
	
	@XmlElement(name = "newPassword")
	private String newPassword;
	
	@XmlElement(name = "digest")
	private String digest;
	
	public ResetForgottenPasswordRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}


	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	
	
	
}
