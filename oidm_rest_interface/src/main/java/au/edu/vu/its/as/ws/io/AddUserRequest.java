package au.edu.vu.its.as.ws.io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import au.edu.vu.its.as.ws.io.AbstractRequest;
import au.edu.vu.its.as.presn.dto.oidm.StaffDTO;

@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddUserRequest extends AbstractRequest {
	
	@XmlElement(name = "staff")
	private StaffDTO staff;
	
	@XmlElement(name = "adminId")
	private String adminId;
	
	@XmlElement(name = "digest")
	private String digest;

	public StaffDTO getStaff() {
		return this.staff;
	}

	public void setStaff(StaffDTO staff) {
		this.staff = staff;
	}
	
	public String getAdminId() {
		return adminId;
	}
	
	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getDigest() {
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	
}
