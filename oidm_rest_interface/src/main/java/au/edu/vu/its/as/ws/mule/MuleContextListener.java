package au.edu.vu.its.as.ws.mule;

import javax.crypto.SecretKey;

import org.mule.api.context.notification.MuleContextNotificationListener;
import org.mule.api.registry.RegistrationException;
import org.mule.context.notification.MuleContextNotification;
import org.mule.module.client.MuleClient;
import org.mule.api.MuleContext;
import org.mule.api.MuleException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import au.edu.vu.its.as.util.CryptoUtil;




@Component
public class MuleContextListener  implements MuleContextNotificationListener<MuleContextNotification> {
	public static boolean initialized = false;
	public static MuleContext mctx = null;
	public static MuleClient muleClient = null;
	public static SecretKey VU_KEY = null;
	public static SecretKey passPharse = null;
	@Value("${oidm.env}")
	public static String env;
	@Value("${oidm.passPhrase}")
	public static String passPhrase;

	
	public static String getEnv() {
		return env;
	}

	public static void setEnv(String env) {
		MuleContextListener.env = env;
	}
	
	public static String getPassPhrase() {
		return passPhrase;
	}

	public static void setPassPhrase(String passPhrase) {
		MuleContextListener.passPhrase = passPhrase;
	}
	

	@Override
	public void onNotification(MuleContextNotification notification) {

		if (notification.getAction() == MuleContextNotification.CONTEXT_STARTED
				&& !initialized) {
			initialized = true;
			System.out.println("Mule context started..........");
			
			mctx = notification.getMuleContext();
			
			try {
				muleClient = new MuleClient(mctx);
			} catch (MuleException e) {
				System.out.println("Could not create mule client:" + e);
				e.printStackTrace();
			}
			System.out.println("mule client initialized..........");
			
			try {
				VU_KEY = CryptoUtil.getKeyFile(Thread.currentThread().getContextClassLoader()
						.getResource("myVUKey").getPath());
			} catch (Exception e) {
				System.out.println("Getting VU_KEY file failed:" + e);
				e.printStackTrace();
			}
			
			System.out.println("Getting VU_KEY done.......... " + Thread.currentThread().getContextClassLoader()
					.getResource("myVUKey").getPath());
		}
	}
	
}
