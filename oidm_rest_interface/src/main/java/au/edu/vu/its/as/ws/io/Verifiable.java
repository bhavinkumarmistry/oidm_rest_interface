package au.edu.vu.its.as.ws.io;

public interface Verifiable {

	String[] getParameters();
	
	String getDigest();
	
}
