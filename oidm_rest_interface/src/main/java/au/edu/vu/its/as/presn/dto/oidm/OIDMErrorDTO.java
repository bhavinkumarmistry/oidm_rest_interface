package au.edu.vu.its.as.presn.dto.oidm;

import java.io.Serializable;

public class OIDMErrorDTO implements Serializable {
      private String code;
      private String message;
      
      public String getCode() {
            return code;
      }
      
      public void setCode(String code) {
            if (null != code) {
                  this.code = code;
            }
      }

      public String getMessage() {
            return message;
      }
      
      public void setMessage(String message) {
            this.message = message;
      }
      
      @Override
      public String toString() {
            return "OIDMErrorDTO [code=" + code + ", message=" + message + "]";
      }

}

