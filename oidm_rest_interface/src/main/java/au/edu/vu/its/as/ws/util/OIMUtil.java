package au.edu.vu.its.as.ws.util;

import au.edu.vu.its.as.util.CryptoUtil;
import au.edu.vu.its.as.ws.mule.MuleContextListener;

public class OIMUtil {
	
	public String getEncodedDigest(String[] pParams) throws Exception {
        try {
            return CryptoUtil.encodeSymmetricalPrintable(MuleContextListener.VU_KEY, 
                                                         getDigest(pParams));
        } catch(Exception e) {
            throw new Exception("Exception caught in getEncodedDigest "+e.getMessage());
        }
    }
        


public String getDigest(String[] pParams) throws Exception {
        
        return CryptoUtil.getDigest(pParams,  MuleContextListener.passPhrase);
    }



	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
