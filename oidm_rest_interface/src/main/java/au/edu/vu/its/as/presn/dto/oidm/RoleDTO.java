/*
*
* File Name                      :  RoleDTO.java
* Creation/Modification History  :
*
*  06-May-10  Vladimir Gleiberman  Created
*
*/


package au.edu.vu.its.as.presn.dto.oidm;


import java.util.Date;

/**
 * Copyright (C) 2010 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * This class is used to transfer information about User's Role
 *
 * @author   : Vladimir Gleiberman
 * @version  : 10.0
 */

public class RoleDTO implements java.io.Serializable { 
   
   private String role;
   private Date dateFrom;
   private Date dateTo;
      

   /**
    * Returns User Role
    *
    * @return <b>role</b> User Role
   */
   public String getRole() {
      return role;
   }

   
   /**
    * Returns User Role Start date
    *
    * @return <b>dateFrom</b> user Role Start date
   */
   public Date getDateFrom() {
      return dateFrom;
   }

   
   /**
    * Returns User Role End date
    *
    * @return <b>dateTo</b> User Role End date
   */
   public Date getDateTo() {
      return dateTo;
   }
   

   /**
    * Sets the value of Role
    *
    * @param  <b>pRole</b> User Role
   */
   public void setRole(String pRole) {
      role = pRole;
   }

   
   /**
    * Sets the value of Role Start date
    *
    * @param  <b>pDateFrom</b> Yser Role Start date
   */
   public void setDateFrom(Date pDateFrom) {
      dateFrom = pDateFrom;
   }

   
   /**
    * Sets the value of Role End date
    *
    * @param  <b>pDateTo</b> User Role End date
   */
   public void setDateTo(Date pDateTo) {
      dateTo = pDateTo;
   }
   

   /**
    * Returns the string representation of this Class.
    *
    * @return <b>String</b> printable version of an instance
    */
   public String toString() {
      return "role=" + getRole() + " DateFrom=" + getDateFrom() +
                                   " DateTo=" + getDateTo() ;
   }
}
