package au.edu.vu.its.oidm.ldap.transformer;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.module.ldap.api.LDAPEntry;
import org.mule.module.ldap.api.LDAPEntryAttribute;
import org.mule.transformer.AbstractMessageTransformer;
import org.mule.transformer.types.DataTypeFactory;

import au.edu.vu.its.as.presn.dto.oidm.AuthenticationDTO;
import au.edu.vu.its.as.presn.dto.oidm.RoleDTO;
import au.edu.vu.its.as.presn.dto.oidm.StaffDTO;

public class LDAPToObjectTransformet extends AbstractMessageTransformer {

	protected static final SimpleDateFormat ROLE_DATE_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
	protected static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyyMMdd");

	public LDAPToObjectTransformet() {
		registerSourceType(DataTypeFactory.create(List.class, LDAPEntry.class));
		setReturnDataType(DataTypeFactory.create(StaffDTO.class));
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		final List<LDAPEntry> ldapEntries = (List<LDAPEntry>) message.getPayload();
		StaffDTO staff = new StaffDTO();
		
		ldapEntries.stream().filter(lc -> !lc.isEmpty()).forEach(lc -> {
			staff.setLoginId(emptyCheck(lc.getAttributes().getAttribute("uid")));
			staff.setVuId(emptyCheck(lc.getAttributes().getAttribute("employeeNumber")));
			staff.setFirstName(emptyCheck(lc.getAttributes().getAttribute("givenName")));
			staff.setFamilyName(emptyCheck(lc.getAttributes().getAttribute("sn")));
			staff.setSecondName(emptyCheck(lc.getAttributes().getAttribute("vuMiddleName")));
			staff.setCampusIdSMS(emptyCheck(lc.getAttributes().getAttribute("vuCampusIdSMS")));
			staff.setCampusIdHR(emptyCheck(lc.getAttributes().getAttribute("vuCampusIdHR")));
			staff.setFacultyIdHR(emptyCheck(lc.getAttributes().getAttribute("vuFacultyHR")));
			staff.setFacultyIdSMS(emptyCheck(lc.getAttributes().getAttribute("vuFacultySMS")));
			staff.setDepartmentIdHR(emptyCheck(lc.getAttributes().getAttribute("vuDepartmentHR")));
			staff.setDepartmentIdSMS(emptyCheck(lc.getAttributes().getAttribute("vuDepartmentSMS")));
			staff.setCostGroup(emptyCheck(lc.getAttributes().getAttribute("vuCostGroup")));
			staff.setExtension(emptyCheck(lc.getAttributes().getAttribute("vuExtension")));
			staff.setPositionType(emptyCheck(lc.getAttributes().getAttribute("vuPositionType")));
			staff.setFaxNumber(emptyCheck(lc.getAttributes().getAttribute("facsimileTelephoneNumber")));
			staff.setEmail(emptyCheck(lc.getAttributes().getAttribute("mail")));
			staff.setTitle(emptyCheck(lc.getAttributes().getAttribute("personalTitle")));
			staff.setPositionTitle(emptyCheck(lc.getAttributes().getAttribute("title")));
			staff.setLibPatronCode(emptyCheck(lc.getAttributes().getAttribute("vuLibPatronCode")));
			staff.setAuthoritativeSource(emptyCheck(lc.getAttributes().getAttribute("vuAuthoritativeSource")));
			staff.setTelephoneNumber(emptyCheck(lc.getAttributes().getAttribute("telephoneNumber")));
			staff.setRoomNumber(emptyCheck(lc.getAttributes().getAttribute("roomNumber")));
			staff.setMobile(emptyCheck(lc.getAttributes().getAttribute("mobile")));
			staff.setMonitorNumber(emptyCheck(lc.getAttributes().getAttribute("vuMonitorNumber")));
			staff.setBarcodeNumber(emptyCheck(lc.getAttributes().getAttribute("vuBarcodeNumber")));
			staff.setManagerId(emptyCheck(lc.getAttributes().getAttribute("vuManagerId")));
			staff.setCostCentre(emptyCheck(lc.getAttributes().getAttribute("vuCostCentres")));
			String t = emptyCheck(lc.getAttributes().getAttribute("vuEndOfContract"));
			try {
				if (null != t) {
					staff.setEndOfContract(DATE_FORMATTER.parse(t.substring(0, 8)));
				}
				t = emptyCheck(lc.getAttributes().getAttribute("vuExpiryDate"));
				if (null != t) {
					staff.setExpiryDate(DATE_FORMATTER.parse(t.substring(0, 8)));
				}
				t = emptyCheck(lc.getAttributes().getAttribute("vuTerminationDate"));
				if (null != t) {
					staff.setTerminationDate(DATE_FORMATTER.parse(t.substring(0, 8)));
				}
				t = emptyCheck(lc.getAttributes().getAttribute("vuLeaveHours"));
				if (null != t) {
					staff.setLeaveHours(Integer.parseInt(t));
				}
				t = emptyCheck(lc.getAttributes().getAttribute("vuFte"));
				if (null != t) {
					staff.setFte(new BigDecimal(t));
				}
			} catch (Exception ex) {
				throw new RuntimeException(ex.getMessage(), ex);
			}

			AuthenticationDTO authInfo = new AuthenticationDTO();
			authInfo.setDefaultRole(emptyCheck(lc.getAttributes().getAttribute("vuDefaultRole")));
			authInfo.setRoles(extractRoles(emptyCheck(lc.getAttributes().getAttribute("vuRoleLists"))));
			staff.setAuthenticationInfo(authInfo);
		});
		return staff;
	}

	private String emptyCheck(LDAPEntryAttribute attribute) {
		if (attribute == null) {
			return null;
		} else {
			return attribute.getValue().toString();
		}
	}

	/**
	 * Extract Roles information from Responce
	 * 
	 * @param <b>pResponse</b>
	 *            IDM SPML Response
	 * @return <b>RoleDTO[]</b> Roles
	 */
	public RoleDTO[] extractRoles(String pRoleList) {

		if (null == pRoleList) {
			return new RoleDTO[0];
		}

		String[] rs = pRoleList.split(",");
		int n = rs.length;
		RoleDTO[] roles = new RoleDTO[n];
		for (int i = 0; i < n; i++) {
			roles[i] = new RoleDTO();
			String[] oneRole = rs[i].split(";");
			roles[i].setRole(oneRole[0]);
			int m = oneRole.length;
			if (m > 1 && null != oneRole[1]) {
				try {
					roles[i].setDateFrom(ROLE_DATE_FORMATTER.parse(oneRole[1]));
				} catch (Exception ignore) {
				}
			}
			if (m > 2 && null != oneRole[2]) {
				try {
					roles[i].setDateTo(ROLE_DATE_FORMATTER.parse(oneRole[2]));
				} catch (Exception ignore) {
				}
			}
		}

		return roles;
	}
}
