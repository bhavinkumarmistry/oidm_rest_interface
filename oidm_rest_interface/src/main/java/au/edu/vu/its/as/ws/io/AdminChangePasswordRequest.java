package au.edu.vu.its.as.ws.io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;

import au.edu.vu.its.as.ws.io.AbstractRequest;


@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class AdminChangePasswordRequest extends AbstractRequest {

	@XmlElement(name = "userId")
	private String userId;
	
	@XmlElement(name = "adminId")
	private String adminId;
	
	@XmlElement(name = "newPassword")
	private String newPassword;
	
	private String digest;
	
	public AdminChangePasswordRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getAdminId() {
		return adminId;
	}

	public void setAdminId(String adminId) {
		this.adminId = adminId;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	
	public String getDigest() {
		// TODO Auto-generated method stub
		return digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}
	
	
}
