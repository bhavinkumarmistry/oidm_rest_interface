package au.edu.vu.its.as.ws.io;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public abstract class AbstractRequest {

	protected class ParameterListBuilder {
		
		private List<String> list;
		
		public ParameterListBuilder() {
			list = new ArrayList<String>();
		}
		
		public ParameterListBuilder add(String str) {
			list.add(StringUtils.defaultString(str));
			return this;
		}
		
		public String[] toArray() {
			return list.toArray(new String[list.size()]);
		}
	}
}
