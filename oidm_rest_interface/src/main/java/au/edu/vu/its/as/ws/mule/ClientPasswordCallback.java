/*
*
* File Name                      :  ClientPasswordCallback.java
* Creation/Modification History  :
*
*   04-Oct-2010  Vladimir GLEIBERMAN  Created
*   05-Jan-2011  Vladimir GLEIBERMAN  Passwords from properties file
*
*/

package au.edu.vu.its.as.ws.mule;

import java.io.IOException;
import java.util.Properties;

import javax.security.auth.callback.Callback;
import javax.security.auth.callback.CallbackHandler;
import javax.security.auth.callback.UnsupportedCallbackException;

import org.apache.ws.security.WSPasswordCallback;

/**
 * Copyright (C) 2010-2011 Victoria University, Melbourne, VIC AUS All Rights
 * Reserved.
 *
 * Provides password for secure WS calls
 *
 * @author : Vladimir GLEIBERMAN
 * @version : 11.1
 */
public class ClientPasswordCallback implements CallbackHandler {

	private static Properties passwords = null; // WS passwords

	/**
	 * Initialises list of WS Passwords
	 *
	 * @param <b>pPasswords</b>
	 *            WS passwords
	 */
	public static void init(Properties pPasswords) {
		passwords = pPasswords;
	}

	/**
	 * Provides Password for a given User Id
	 *
	 * @param <b>pCallbacks</b>
	 *            Ids requiring Passwords
	 * @throw <b>Exceptions</b> as required per overriden method
	 */
	public void handle(Callback[] pCallbacks) throws IOException, UnsupportedCallbackException {
		System.out.println("OIM### password call back");
		WSPasswordCallback pc = (WSPasswordCallback) pCallbacks[0];
		String id = pc.getIdentifier(); // Name for password
		System.out.println("OIM### password call back ID = " + id);
		System.out.println("OIM### password call back Password = " + passwords.getProperty(id));
		pc.setPassword(passwords.getProperty(id)); // Requested password
		// pc.setPassword("ESBrn2013");

	}
}
