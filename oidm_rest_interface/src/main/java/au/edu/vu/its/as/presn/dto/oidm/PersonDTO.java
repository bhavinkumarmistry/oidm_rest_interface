/*
 *
 * File Name                      :  PersonDTO.java
 * Creation/Modification History  :
 *
 * 15-Feb-10  Andrew RUAN  Created
 * 24-Mar-10  Andrew RUAN  Added passwordStatus attribute
 * 19-Apr-10  Andrew RUAN  Changed passwordStatus to authenticationInfo
 *                         Removed defaultRole
 * 30-05-2013 Chelladurai KANAGARATNAM   CostCentre added
 * 10--7-2013 Chelladurai KANAGARATNAM   personalEmail added
 */
package au.edu.vu.its.as.presn.dto.oidm;

import java.util.Date;

/**
 * Copyright (C) 2010 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * This class used to transfer information b/w presentation/business layers
 *
 * @author   : Andrew RUAN
 * @Version  : 10.1
 */

public class PersonDTO implements java.io.Serializable {

   private String            firstName;
   private String            vuId;
   private String            familyName;
   private String            loginId;
   private String            title;
   private String            email;
   private String            personalEmail;
   private String            location;
   private String            libPatronCode;
   private Date              expiryDate;
   private String            costCentre;
   private String            authoritativeSource;
   private AuthenticationDTO authenticationInfo;


   /**
    * Returns firstName value
    * 
    * @return <b>firstName</b> firstName value
    */
   public String getFirstName() {
      return firstName;
   }


   /**
    * Sets the value of firstName
    * 
    * @param <b>pFirstName</b> firstName value
    */
   public void setFirstName(String pFirstName) {
      firstName = pFirstName;
   }


   /**
    * Returns vuId value
    * 
    * @return <b>vuId</b> vuId value
    */
   public String getVuId() {
      return vuId;
   }


   /**
    * Sets the value of vuId
    * 
    * @param <b>pVuId</b> vuId value
    */
   public void setVuId(String pVuId) {
      vuId = pVuId;
   }


   /**
    * Returns costCentre value
    * 
    * @return <b>costCentre</b> costCentre value
    */
   public String getCostCentre() {
      return costCentre;
   }


   /**
    * Sets the value of costCentre
    * 
    * @param <b>pCostCentre</b> costCentre value
    */
   public void setCostCentre(String pCostCentre) {
      costCentre = pCostCentre;
   }



   /**
    * Returns familyName value
    * 
    * @return <b>familyName</b> familyName value
    */
   public String getFamilyName() {
      return familyName;
   }


   /**
    * Sets the value of familyName
    * 
    * @param <b>pFamilyName</b> familyName value
    */
   public void setFamilyName(String pFamilyName) {
      familyName = pFamilyName;
   }


   /**
    * Returns loginId value
    * 
    * @return <b>loginId</b> loginId value
    */
   public String getLoginId() {
      return loginId;
   }


   /**
    * Sets the value of loginId
    * 
    * @param <b>ploginId</b> loginId value
    */
   public void setLoginId(String pLoginId) {
      loginId = pLoginId;
   }


   /**
    * Returns title value
    * 
    * @return <b>title</b> title value
    */
   public String getTitle() {
      return title;
   }


   /**
    * Sets the value of title
    * 
    * @param <b>pTitle</b> title value
    */
   public void setTitle(String pTitle) {
      title = pTitle;
   }



   /**
    * Returns email value
    * 
    * @return <b>email</b> email value
    */
   public String getEmail() {
      return email;
   }


   /**
    * Sets the value of email
    * 
    * @param <b>pEmail</b> email value
    */
   public void setEmail(String pEmail) {
      email = pEmail;
   }

   /**
    * Returns personal email value
    * 
    * @return <b>personal email</b> personal email value
    */
   public String getPersonalEmail() {
      return personalEmail;
   }


   /**
    * Sets the value of Personalemail
    * 
    * @param <b>pPersonalEmail</b> Personalemail value
    */
   public void setPersonalEmail(String pPersonalEmail) {
      personalEmail = pPersonalEmail;
   }


   /**
    * Returns location value
    * 
    * @return <b>location</b> location value
    */
   public String getLocation() {
      return location;
   }


   /**
    * Sets the value of location
    * 
    * @param <b>pLocation</b> location value
    */
   public void setLocation(String pLocation) {
      location = pLocation;
   }


   /**
    * Returns libPatronCode value
    * 
    * @return <b>libPatronCode</b> libPatronCode value
    */
   public String getLibPatronCode() {
      return libPatronCode;
   }


   /**
    * Sets the value of libPatronCode
    * 
    * @param <b>pLibPatronCode</b> libPatronCode value
    */
   public void setLibPatronCode(String pLibPatronCode) {
      libPatronCode = pLibPatronCode;
   }


   /**
    * Returns expiryDate value
    * 
    * @return <b>expiryDate</b> expiryDate value
    */
   public Date getExpiryDate() {
      return expiryDate;
   }


   /**
    * Sets the value of expiryDate
    * 
    * @param <b>pExpiryDate</b> expiryDate value
    */
   public void setExpiryDate(Date pExpiryDate) {
      expiryDate = pExpiryDate;
   }


   /**
    * Returns authoritativeSource value
    * 
    * @return <b>authoritativeSource</b> authoritativeSource value
    */
   public String getAuthoritativeSource() {
      return authoritativeSource;
   }


   /**
    * Sets the value of authoritativeSource
    * 
    * @param <b>pAuthoritativeSource</b> authoritativeSource value
    */
   public void setAuthoritativeSource(String pAuthoritativeSource) {
      authoritativeSource = pAuthoritativeSource;
   }

   
   /**
    * Returns authenticationInfo value
    *
    * @return <b>authenticationInfo</b> authenticationInfo value
    */
   public AuthenticationDTO getAuthenticationInfo() {
      return authenticationInfo;
   }


   /**
    * Sets the value of authenticationInfo
    *
    * @param <b>pAuthenticationInfo</b> authenticationInfo value
    */
   public void setAuthenticationInfo(AuthenticationDTO pAuthenticationInfo) {
      authenticationInfo = pAuthenticationInfo;
   }


   /**
    * Returns String
    * 
    * @return <b>String</b> String
    */
   public String toString() {
      StringBuilder str = new StringBuilder();
      str.append("Person[");
      str.append("firstName = ").append(firstName);
      str.append(", vuId = ").append(vuId);
      str.append(", familyName = ").append(familyName);
      str.append(", loginId = ").append(loginId);
      str.append(", title = ").append(title);
      str.append(", email = ").append(email);
      str.append(", personal email = ").append(personalEmail);
      str.append(", location = ").append(location);
      str.append(", libPatronCode = ").append(libPatronCode);
      str.append(", expiryDate = ").append(expiryDate);
      str.append(", costCentre = ").append(costCentre);
      str.append(", authoritativeSource = ").append(authoritativeSource);
      str.append(", authenticationInfo = ").append(authenticationInfo);
      str.append("]");
      return str.toString();
   }

}
