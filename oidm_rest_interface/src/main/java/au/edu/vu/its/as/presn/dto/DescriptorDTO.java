/*
*
* File Name                      :  DescriptorDTO.java
* Creation/Modification History  :
*
*  10-Apr-03  Chelladurai KANAGARATNAM  Created
*
*/

package au.edu.vu.its.as.presn.dto;


/**
 * Copyright (C) 2004 Victoria University, Melbourne, VIC AUS
 * All Rights Reserved.
 *
 * This generic class can be used for any object which has two attributes .
 *
 * @author   : Chelladurai KANAGARATNAM
 * @Version  : 2.0
 */

public class DescriptorDTO implements java.io.Serializable {
    
    private String id;
    private String description;
    
   /** 
    * Returns Id 
    *
    * @return <b>Id</b>  Id
    */ 
   public String getId() {
      return id;
   }
    
   /** 
    * Returns description 
    *
    * @return <b>description</b>  description
    */ 
   public String getDescription() {
      return description;
   }
    
   /** 
    * Set the value of Id 
    *
    * @param <b>pId</b>  Id
    */ 
   public void setId(String pId) {
      id = pId;
   }
    
   /** 
    * Set the value of Description 
    *
    * @param <b>pDescription</b>  Description
    */ 
   public void setDescription(String pDescription) {
      description = pDescription;
   }
    
   /** 
    * Returns String 
    * @return <b>String</b> printable version of an instance
    */ 
   public String toString() {
      return "Id: " + id + " = description: " + description; 
   }
}
