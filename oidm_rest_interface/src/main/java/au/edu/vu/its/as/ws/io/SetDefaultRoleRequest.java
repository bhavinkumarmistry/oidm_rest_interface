package au.edu.vu.its.as.ws.io;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import au.edu.vu.its.as.ws.transformer.*;

@XmlRootElement(name = "req")
@XmlAccessorType(XmlAccessType.FIELD)
public class SetDefaultRoleRequest  extends AbstractRequest {

	@XmlElement(name = "userId")
	private String userId;
	
	@XmlElement(name = "defaultRole")
	private String defaultRole;
	
	public SetDefaultRoleRequest() {
		// TODO Auto-generated constructor stub
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getDefaultRole() {
		return defaultRole;
	}

	public void setDefaultRole(String defaultRole) {
		this.defaultRole = defaultRole;
	}
	
}
